function out=A_sj(T,P,sigx,sigy)
out=1/(2*pi*sigx*sigy)*exp(-0.5*((T/sigx).^2+(P/sigy).^2));