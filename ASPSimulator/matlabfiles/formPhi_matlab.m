function PHI = formPhi_matlab(patchSize,ky,kx,bRecoverDiff) 
    % ASP patches for averaging light field angles
                    % R = ASPProjMatrix(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
    [R1, R2] = ASPProjMatrixTwoPhase(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
    R = [R1(:); R2(:);];
    
    % construct measurement matrix from random angle averaging
    if bRecoverDiff==1
    PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
        count = 1;
        offset = prod(patchSize);
        for kk=1:patchSize(3)*patchSize(4)
            PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) - R(count:count+patchSize(1)*patchSize(2)-1);
            count = count + patchSize(1)*patchSize(2);
        end
    
        count = 1;
        for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
            PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                (R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) + R(count:count+patchSize(1)*patchSize(2)-1)) / 8;
            count = count + patchSize(1)*patchSize(2);
        end
    else
    PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
    count = 1;
    for kk=1:patchSize(3)*patchSize(4)
        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
            R(count:count+patchSize(1)*patchSize(2)-1);
        count = count + patchSize(1)*patchSize(2);
    end
    count = 1;
    offset = prod(patchSize);
    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1);
        count = count + patchSize(1)*patchSize(2);
    end
    end
end
