%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Load a 4D test light field from file, simulate coded projection onto a
%   2D sensor image, and recover the 4D light field.
%
%   Please note that this simulation does not exactly match physically
%   realizable constraints. For each sensor pixel, all incident angles of
%   the light field are randomly modulated and summed. The optical setup
%   described in the SIGGRAPH 2013 paper "Compressive Light Field
%   Photography using Overcomplete Dictionaries and Optimized Projections"
%   uses a coded attenuation mask in between sensor and camera aperture. As
%   such, light field angles are averaged, but the mask code is not random
%   but periodic over the sensor image. The setup simulated in this code is
%   not exactly what we used in the paper, but more more intuitive and
%   easier to use.
%
%   Gordon Wetzstein [gordonw@media.mit.edu]
%   Research Scientist
%   MIT Media Lab . Camera Culture Group
%   media.mit.edu/~gordonw
%   July 2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clf;

addpath('..\3rdparty\spgl1-1.7');
addpath('..\3rdparty\');



% recover only a small test region
bTestRegion = true;

% 0 is patch-by-patch (faster), 1 is sliding patches (much slower)
patchMode = 0;

% 0 is for dual phase reconstruction, 1 is for differential reconstruction
bRecoverDiff = 0;

% load dictionary
load('..\DictionaryLearning\LightField4D-Dictionary_9x9.mat'); % this is a better dictionary

% max iterations for optimization
maxIters = 100*patchSize(3)*patchSize(4);

% print progress and current error
echoProgress = true;

% grayscale (faster)
bGrayscale = true;

% solver:
%   0 - SPGL1
%   1 - ADMM
bReconstructionMethod = 1;

% noise: 
%  0 - no noise
%  1 - add gaussian noise
bNoise = 0;
gnsigma = 0.1;

% set global lambda for ADMM
%admmlambda = 0.01;
%new value tuned by Gordon
admmlambda = 0.001;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% filename

saveLocation = 'LightField-Reconstruction-Synthetic';

saveSuffix = '';

if(patchMode == 1)
    saveSuffix = strcat('-SlidingPatch',saveSuffix);
elseif (patchMode == 0)
	saveSuffix = strcat('-PatchByPatch',saveSuffix);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load original light field

% number of pixels in [angle_y angle_x spatial_y spatial_x numColorChannels]
lightFieldResolution = [5 5 593 840 3];
lightField = zeros(lightFieldResolution);
addpath('..\Light_Fields\DragonsAndBunnies_5x5_ap6.6\');
for ky=1:lightFieldResolution(1)
    for kx=1:lightFieldResolution(2)
        I = im2double(imread(sprintf('dragons-%.2d.png', (ky-1)*lightFieldResolution(2)+kx)));
        lightField(ky,kx,:,:,:) = reshape(I, [1 1 lightFieldResolution(3) lightFieldResolution(4) 3]);
    end
end


%uncomment for FISHI lightfield
% lightFieldResolution = [5 5 593 840 3]; %for FISHI
% lightField = zeros(lightFieldResolution);
% addpath('..\Light_Fields\fishi_camera\fishi_5x5_ap35\');
% for ky=1:lightFieldResolution(1)
%     for kx=1:lightFieldResolution(2)
%         I = im2double(imread(sprintf('fishi-%.2d.png', (ky-1)*lightFieldResolution(2)+kx)));
%         lightField(ky,kx,:,:,:) = reshape(I, [1 1 lightFieldResolution(3) lightFieldResolution(4) 3]);
%     end
% end

% lightFieldResolution = [5 5 525 840 3]; %for FISHI Display FOV30
% lightField = zeros(lightFieldResolution);
% addpath('..\Light_Fields\fishi\FOV10\');
% for ky=1:lightFieldResolution(1)
%     for kx=1:lightFieldResolution(2)
%         I = im2double(imread(sprintf('fishi-%.2d.png', (ky-1)*lightFieldResolution(2)+kx)));
%         lightField(ky,kx,:,:,:) = reshape(I, [1 1 lightFieldResolution(3) lightFieldResolution(4) 3]);
%     end
% end

% lightFieldResolution = [5 5 240 320 3]; %for Vase lightfield
% lightField = zeros(lightFieldResolution);
% addpath('..\Light_Fields\vase lightfield\');
% for ky=1:lightFieldResolution(1)
%     for kx=1:lightFieldResolution(2)
%         I = im2double(imread(sprintf('demo1%.2d.png', (ky-1)*lightFieldResolution(2)+kx)));
%         lightField(ky,kx,:,:,:) = reshape(I, [1 1 lightFieldResolution(3) lightFieldResolution(4) 3]);
%     end
% end
%     lightField = lightField(:,:,100:150,140:240,:);
%     
%     lightFieldResolution = size(lightField);


numColorChannels = 3;
if bGrayscale
    lightField = mean(lightField,5);
    lightFieldResolution = size(lightField);
    numColorChannels = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% small region
if bTestRegion
    
    padSize = [patchSize(3), patchSize(4)];
    RegionSize = [75, 100];

%     Region1 = [205, 96];
%     lightField = lightField(:,:,Region1(1)-padSize(1):Region1(1)+RegionSize(1)+padSize(1), ...
%                                 Region1(2)-padSize(2):Region1(2)+RegionSize(2)+padSize(2),:);
%     saveSuffix = strcat('-patch1',saveSuffix);
%     
    Region2 = [205,723];
    lightField = lightField(:,:,Region2(1)-padSize(1):Region2(1)+RegionSize(1)+padSize(1), ...
                                Region2(2)-padSize(2):Region2(2)+RegionSize(2)+padSize(2),:);
    saveSuffix = strcat('-patch2',saveSuffix);

%     Region3 = [315,343];
%     lightField = lightField(:,:,Region3(1)-padSize(1):Region3(1)+RegionSize(1)+padSize(1), ...
%                                 Region3(2)-padSize(2):Region3(2)+RegionSize(2)+padSize(2),:);
%     saveSuffix = strcat('-patch3',saveSuffix);

    % Gordon's test patch
%    lightField = lightField(:,:,90:180,120:240,:);
    
    lightFieldResolution = size(lightField);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% patch-by-patch reconstruction (fast)

if patchMode == 0

    numImagePatches = [floor(lightFieldResolution(3)/patchSize(3)) floor(lightFieldResolution(4)/patchSize(4))];
    
    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);
    
    % reconstruct all the patches
    totalTime = tic;
  
    
    for c=1:numColorChannels
        for ky=1:numImagePatches(1)
            for kx=1:numImagePatches(2)
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % original patch
                Pori = lightField(:,:, (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4), c);
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % project down to 2D measurement
                
                % random values for averaging light field angles
                %R = rand(patchSize);
                %R = (randn(patchSize)./2)+0.5; R(R<0)=0; R(R>1)=1;
                %R = R(:);
                
                % ASP patches for averaging light field angles
                % R = ASPProjMatrix(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
                [R1, R2] = ASPProjMatrixTwoPhase(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
                R = [R1(:); R2(:);];
                
                % construct measurement matrix from random angle averaging
                if bRecoverDiff==1
                PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                    count = 1;
                    offset = prod(patchSize);
                    for kk=1:patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) - R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end

                    count = 1;
                    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            (R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) + R(count:count+patchSize(1)*patchSize(2)-1)) / 8;
                        count = count + patchSize(1)*patchSize(2);
                    end
                else
                PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                count = 1;
                for kk=1:patchSize(3)*patchSize(4)
                    PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                        R(count:count+patchSize(1)*patchSize(2)-1);
                    count = count + patchSize(1)*patchSize(2);
                end
                count = 1;
                offset = prod(patchSize);
                for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                    PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                        R(count+offset:count+offset+patchSize(1)*patchSize(2)-1);
                    count = count + patchSize(1)*patchSize(2);
                end
                end
                % measurement on sensor
                I = PHI*Pori(:);
                
                if(bNoise==1) % add gaussian noise
                    n = gnsigma*randn(size(I));
                    n(n>1)=1;
                    n(n<0)=0;
                    I = I + n;
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % reconstruct patch with SPGL1
                
                if bReconstructionMethod == 0
                    
                    sigma = 0.001;    % Desired ||Ax - b||_2
                    
                    % options
                    opts = spgSetParms( 'verbosity', 0,...
                        'iterations', maxIters );
                    
                    % reconstruct sparse coefficients
                    currentTime = tic;
                    alpha   = spg_bpdn(PHI*D, I, sigma, opts);
                    timeElapsed = toc(currentTime);
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % reconstruct patch with ADMM
                    
                else
                    
                     currentTime = tic;
                    alpha = lassoADMM(PHI*D, I, admmlambda, 1.0, 1.0);
                     timeElapsed = toc(currentTime);
                    
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                % reconstruct patch
                Prec    = D*alpha;
              
                
                % plot progress and error
                if echoProgress
                    MSR  = mean( (Pori(:)-Prec).^2 );
                    PSNR = 10 * log10( 1/MSR );
                    disp(['patch ' num2str( (c-1)*numImagePatches(2)*numImagePatches(1) + (ky-1)*numImagePatches(2) + kx) ' of ' num2str(numImagePatches(1)*numImagePatches(2)*numColorChannels) ', took (in ms): ' num2str(timeElapsed) ', PSNR (in dB): ' num2str(PSNR)]);
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % sort into reconstruction
                lightFieldRec(:,:, (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4), c) = reshape(Prec, patchSize);
                
              
            end
        end
    end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
    % save data
    
    save(strcat(saveLocation,saveSuffix,'.mat'), 'lightField', 'lightFieldRec','elapsedTimeTotal', 'patchSize');
else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % sliding patch reconstruction (very slow)
    
    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);
    
    numImagePatches = [lightFieldResolution(3)-patchSize(3) lightFieldResolution(4)-patchSize(4)];
    
    if(bNoise==1) % add gaussian noise
        n = gnsigma*randn(size(lightField));
        n(n>1)=1;
        n(n<0)=0;
    end
                
    % reconstruct all the patches
    totalTime = tic;
    for ii=1:numColorChannels*numImagePatches(1)*numImagePatches(2)
        
       
        % current color channel
        c  = floor( (ii-1) / (numImagePatches(1)*numImagePatches(2)) ) + 1;
        % current patch index in y
        ky = floor( ((ii - (c-1)*numImagePatches(1)*numImagePatches(2))-1) / numImagePatches(2) ) + 1;
        % current patc hindex in x
        kx = mod(ii-1,numImagePatches(2))+1;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % original patch
        Pori = lightField(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c);
        
        if(bNoise>0) % add the noise
            Pori = Pori + n(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % project down to 2D measurement
        
        % randome values for averaging light field angles
        %R = rand(patchSize);
        %R = R(:);
        
        % ASP
        %R = ASPProjMatrix(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
        [R1, R2] = ASPProjMatrixTwoPhase(patchSize,(ky-1)*patchSize(3)+1,(kx-1)*patchSize(4)+1);
        R = [R1(:); R2(:);];        
        
        % construct measurement matrix from random angle averaging
        if bRecoverDiff ==1
          PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                    count = 1;
                    offset = prod(patchSize);
                    for kk=1:patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) - R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end

                    count = 1;
                    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            (R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) + R(count:count+patchSize(1)*patchSize(2)-1)) / 8;
                        count = count + patchSize(1)*patchSize(2);
                    end
        else
                   
        PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
        count = 1;
        for kk=1:patchSize(3)*patchSize(4)
            PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                R(count:count+patchSize(1)*patchSize(2)-1);
            count = count + patchSize(1)*patchSize(2);
        end
        count = 1;
        offset = prod(patchSize);
        for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
            PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                R(count+offset:count+offset+patchSize(1)*patchSize(2)-1);
            count = count + patchSize(1)*patchSize(2);
        end
        end
        % measurement on sensor
        I = PHI*Pori(:);
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % reconstruct patch with SPGL1
        
        if bReconstructionMethod == 0
            
            sigma = 0.01;    % Desired ||Ax - b||_2
            
            % options
            opts = spgSetParms( 'verbosity', 0,...
                'iterations', maxIters );
            
            % reconstruct sparse coefficients
            currentTime = tic;
            alpha   = spg_bpdn(PHI*D, I, sigma, opts);
            timeElapsed = toc(currentTime);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % reconstruct patch with ADMM
            
        else
            
            currentTime = tic;
            alpha = lassoADMM(PHI*D, I, admmlambda, 1.0, 1.0);
            timeElapsed = toc(currentTime);
            
        end
        
        % reconstruct patch
        Prec    = D*alpha;
        
        % plot progress and error
        if echoProgress
            MSR  = mean( (Pori(:)-Prec).^2 );
            PSNR = 10 * log10( 1/MSR );
            disp(['patch ' num2str( (c-1)*(lightFieldResolution(3)-patchSize(3))*(lightFieldResolution(4)-patchSize(4)) + (ky-1)*(lightFieldResolution(4)-patchSize(4)) + kx) ' of ' num2str((lightFieldResolution(3)-patchSize(3))*(lightFieldResolution(4)-patchSize(4))*numColorChannels) ', PSNR (in dB): ' num2str(PSNR)]);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % sort into reconstruction
        lightFieldRec(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c) = lightFieldRec(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c) + (1/(patchSize(3)*patchSize(4))).*reshape(Prec, patchSize);
        
       
    end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
    % save data
    save(strcat(saveLocation,saveSuffix,'.mat'), 'lightField', 'lightFieldRec', 'elapsedTimeTotal', 'patchSize');
    
    % show animation of reconstructed light field
    figure; animateLightField(lightFieldRec, 2);
    
end
lightFieldRec = (lightFieldRec - min(lightFieldRec(:)))./(max(lightFieldRec(:))-min(lightFieldRec(:)));
drawLightField4D(lightFieldRec);
figure, animateLightField(lightFieldRec,2);
