function out= Gw_sj(N,m,ASPprms,sc)
% clear;clc;
sigx=sc*pi/15;
sigy=sc*pi/15;
% N=35;
% m=0.5;
% ASPprms = [24,70,0];
b=ASPprms(1);
psi=ASPprms(2);
alpha=ASPprms(3);
t=linspace(-pi/10,pi/10,N);
p=linspace(-pi/10,pi/10,N);
[T,P]=meshgrid(t,p);
Trot=T*cosd(psi)+P*sind(psi);
Prot=-T*sind(psi)+P*cosd(psi);
%out=(m*cos(b*Trot+alpha/90*pi/2));
out=A_sj(Trot,Prot,sigx,sigy)*2.*(m*cos(b*Trot+alpha/90*pi/2));

% out=(1-m*cosd(b*sind(psi)*P+b*cosd(psi)*T+alpha));
% figure(1);
% % surf(T,P,out);
% imagesc(out);colormap('jet');
% avg=mean(mean(out));
% title(['mean = ' num2str(avg)]);
