function [M1, M2] = ASPProjMatrixTwoPhase(patchSize,sy,sx)

%sx - x pos of top left corner of patch
%sy - y pos of top left corner of patch

% Load ASP paramaters
%par = LoadASPpar();
wlow= 1;
wmid= 0.2;%-0.2;
whigh= 0.1;%-0.1;
wDC= 0.005;%0.05;

w = [wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  wlow  ... 
     wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  wmid  ...
     whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh whigh ...
     wDC];

b_low=12;
b_mid=18;
b_high=24;

par(1,:)=   [b_low,0,0];
par(2,:)=   [b_low,0,180];
par(3,:)=   [b_low,0,270];
par(4,:)=   [b_low,0,90];

par(5,:)=   [b_low,90,270];
par(6,:)=   [b_low,90,90];
par(7,:)=   [b_low,90,180];
par(8,:)=   [b_low,90,0];

par(9,:)=   [b_low,45,270];
par(10,:)=  [b_low,45,90];
par(11,:)=  [b_low,45,180];
par(12,:)=  [b_low,45,0];

par(13,:)=  [b_low,-45,270];
par(14,:)=  [b_low,-45,90];
par(15,:)=  [b_low,-45,180];
par(16,:)=  [b_low,-45,0];

par(17,:)=  [b_mid,22.5,180];
par(18,:)=  [b_mid,22.5,0];
par(19,:)=  [b_mid,22.5,270];
par(20,:)=  [b_mid,22.5,90];

par(21,:)=  [b_mid,-67.5,270];
par(22,:)=  [b_mid,-67.5,90];
par(23,:)=  [b_mid,-67.5,180];
par(24,:)=  [b_mid,-67.5,0];

par(25,:)=  [b_mid,-22.5,180];
par(26,:)=  [b_mid,-22.5,0];
par(27,:)=  [b_mid,-22.5,270];
par(28,:)=  [b_mid,-22.5,90];

par(29,:)=  [b_mid,67.5,180];
par(30,:)=  [b_mid,67.5,0];
par(31,:)=  [b_mid,67.5,270];
par(32,:)=  [b_mid,67.5,90];

par(33,:)=  [b_high,0,270];
par(34,:)=  [b_high,0,90];
par(35,:)=  [b_high,0,180];
par(36,:)=  [b_high,0,0];

par(37,:)=  [b_high,90,180];
par(38,:)=  [b_high,90,0];
par(39,:)=  [b_high,90,270];
par(40,:)=  [b_high,90,90];

par(41,:)=  [b_high,45,180];
par(42,:)=  [b_high,45,0];
par(43,:)=  [b_high,45,270];
par(44,:)=  [b_high,45,90];

par(45,:)=  [b_high,-45,180];
par(46,:)=  [b_high,-45,0];
par(47,:)=  [b_high,-45,270]; 
par(48,:)=  [b_high,-45,90];

par(49,:)=  [0,0,0];

nfSort1=[44 40  1 48 12 22;
          4 42  8 28 34 32;
         26 24 16 20 10 38;
         36 46 30  6 18 14;];

nfSort2=[43 39  2 47 11 21;
          3 41  7 27 33 31;
         25 23 15 19  9 37;
         35 45 29  5 17 13;];
     

%nfSort1=[10 40 34 22 24 30;
%         34  1  4 14 28 18;
%         42 20  8 46  6 26;
%         12 48 44 16 32 38];
    
%nfSort2=[ 9 39 33 21 23 29;
%         34  2  3 13 27 17;
%         41 19  7 45  5 25;
%         11 47 43 15 31 37];

    
extSort1 = repmat(nfSort1,ceil(patchSize(3)/size(nfSort1,1)),ceil(patchSize(4)/size(nfSort1,2)));
sortoffsety = mod(sy-1,size(nfSort1,1))+1;
sortoffsetx = mod(sx-1,size(nfSort1,2))+1;
shiftSort1 = extSort1(sortoffsety:sortoffsety+patchSize(3)-1,sortoffsetx:sortoffsetx+patchSize(4)-1);

extSort2 = repmat(nfSort2,ceil(patchSize(3)/size(nfSort2,1)),ceil(patchSize(4)/size(nfSort2,2)));
shiftSort2 = extSort2(sortoffsety:sortoffsety+patchSize(3)-1,sortoffsetx:sortoffsetx+patchSize(4)-1);

M1 = zeros(patchSize);
M2 = zeros(patchSize);

% adjust for aperture size
%th=linspace(80,100,5);
%ph=linspace(80,100,5);


M = 5;   % n samples in response (long axis)
%Mt = M;
%Mp = M;
A = 0.5; % peak amplitude of response
S = 0.7; % scale of gaussian window

% g = ASP_Gw((th-90)*pi/180,(ph-90)*pi/180,0.5,par(49,:),0.7);
g = w(49).*Gw_sj(M,A,par(49,:),S);
for spy = 1:patchSize(3)
    for spx = 1:patchSize(4)
        nASP = shiftSort1(spy,spx);
        %M1(:,:,spy,spx) = g + ASP_Gw((th-90)*pi/180,(ph-90)*pi/180,0.5,par(nASP,:),0.7);
        M1(:,:,spy,spx) = g + w(nASP).*Gw_sj(M,A,par(nASP,:),S);
    end
end
for spy = 1:patchSize(3)
    for spx = 1:patchSize(4)
        nASP = shiftSort2(spy,spx);
        %M2(:,:,spy,spx) = g + ASP_Gw((th-90)*pi/180,(ph-90)*pi/180,0.5,par(nASP,:),0.7);
        M2(:,:,spy,spx) = g + w(nASP).*Gw_sj(M,A,par(nASP,:),S);
    end
end