""" This script
    1 -> reads test data
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Arjun Jauhari/Suren Jayasuriya
#
# Script to load up a network and test it on a sample lightfield

from lfreconpack import inference
from lfreconpack import formPhi as fm

if __name__ == "__main__":
    net = inference.setup_pycaffe()
    inference.do_inference(inference.load_test_lf_syn(fm.formPhiSim), net)
