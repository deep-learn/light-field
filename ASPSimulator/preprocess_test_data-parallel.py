""" This script
    1 -> reads test data
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Arjun Jauhari
#
# Script to load up a network and test it on a sample lightfield
import numpy as np
import math
import cv2
import os
import animateLightField as animLF
from os.path import expanduser
import formPhi as fm
from normalize import normalize
import time
import matplotlib.pyplot as plt
import re
from flags_config import Flags
from flags_config import Config

def dump_data():
    # Print flags
    print "Flags:"
    print "verbose",Flags.verbose
    print "vverbose",Flags.vverbose
    print "animate",Flags.animate
    print "saveanim",Flags.saveanim
    print "overlap",Flags.overlap
    print "normalizePhi",Flags.normalizePhi
    print "subMean",Flags.subMean
    print "diagPhi",Flags.diagPhi

    # load test light field
    patchSize = Config.patchSize
    patchSizefm = Config.patchSize[::-1]
    lightFieldRes = Config.lightFieldRes

    numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
    if Flags.verbose == 1:
        print "Number of Patches: ", numImgPatches

    lightField = np.zeros((1,)+lightFieldRes)
    lightFieldRec = np.zeros((1,)+lightFieldRes)
    lfrMoveValue = np.zeros((1,)+lightFieldRes)
    lfrCount = np.zeros((1,)+lightFieldRes)

    Path = os.path.dirname(os.path.realpath(__file__)) + '/../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'

    trainingLightFieldFilenames = ['fishi_5x5_ap35','messerschmitt_5x5_ap35','shrubbery_5x5_ap35', 'dice_5x5_ap35']
    trainingLFnames = ['fishi-','messerschmitt-','shrubbery-', 'dice-']
    LFnum = 3
    # Load all the images and normalize them
    for ky in range(0,5):
        for kx in range(0,5):
            # read current image
            #I = cv2.imread(Path + trainingLightFieldFilenames[LFnum] + '/' + trainingLFnames[LFnum] + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
            I = cv2.imread(Path + '../DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
            I = I.astype('float64')
            I = I - I.min()
            I = I/(I.max()-I.min())

            lightField[0,:,:,ky,kx] = I

    # (TEST) Animated the loaded lightfield
    if Flags.animate == 1 or Flags.saveanim == 1:
        lf = np.transpose(lightField,(3,4,1,2,0))
        print 'Animating the input: Close the figure to proceed'
        #animLF.animateLightField(lf,'ASPLFOriginal.mp4',Flags.saveanim)

    ############################################################
    # Preparing block diagonal Phi
    ############################################################
    if Flags.diagPhi == 1:
        patchsizeDiag = (5, 5, 1, 1)
        num = 8
        phiBlock = np.zeros((2*num,patchsizeDiag[0]*patchsizeDiag[1]))
        for i in xrange(num):
            yi = i/6 #since both are integers, divide = floor
            xi = np.mod(i,6)
            phiBlock[2*i:2*i+2,:] = fm.formPhiSim(patchsizeDiag,yi,xi,1)

        #print phiBlock.shape
        #hinton.hinton(phiBlock,"phiBlock")
        #hinton.hinton(normalize(phiBlock),"phiBlock")
        #print normalize(phiBlock)

        # Make block diagonal
        Phi = np.kron(np.eye(patchSize[0]*patchSize[1]), phiBlock)
        #print Phi.shape
        #hinton.hinton(Phi,"phiBlock")
    else:
        num = 1

    ############################################################

    # Create patches, send it through the network, and place it in reconstructed light field
    if Flags.overlap == True:
        typ = "overlap"
        # Create Overlapping patches
        for ky in range(0,lightFieldRes[0]-patchSize[0],4):
            if Flags.verbose == 1:
                print 'Processing: %d, %f' % (ky,time.time())
            for kx in range(0,lightFieldRes[1]-patchSize[1],4):

                Patch = lightField[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:]
                vecPatch = np.ravel(Patch)

                # Get the phi corresponding to this patch defined by sy and sx
                if Flags.diagPhi == 0:
                    Phi = fm.formPhiSim(patchSizefm,ky,kx,1)
                if Flags.normalizePhi == 1:
                    Phi = normalize(Phi)

                # simulate measurement on sensor
                measurement = np.dot(Phi,vecPatch)
                # subtract mean
                if Flags.subMean == 1:
                    measurement = measurement - np.mean(measurement)

                ## Change image dimension to match the data blob
                patch_input = measurement[np.newaxis, np.newaxis, :, np.newaxis]
                if Flags.vverbose == 1:
                    print "patch_input",patch_input.shape
                net.blobs['data'].reshape(*patch_input.shape)
                net.blobs['data'].data[...] = patch_input

                ## Call Caffe to Run a forward pass
                net.forward()

                # Initialize variables for intermediate values
                lfrMoveValuetmp = np.zeros((1,)+lightFieldRes)
                lfrCounttmp = np.zeros((1,)+lightFieldRes)

                # Reshape output into patchSize
                lfrMoveValuetmp[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] = np.reshape(net.blobs['r3'].data, patchSize)
                lfrCounttmp[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] = np.ones(patchSize,dtype=np.float64)
                lfrMoveValue += lfrMoveValuetmp
                lfrCount += lfrCounttmp

        # Normalize
        lightFieldRec = lfrMoveValue/lfrCount
        # Setting the nans to 0
        lightFieldRec[np.isnan(lightFieldRec)] = 0
        #np.savez("lfrMoveValue.npz",lfrMoveValue)
        #np.savez("lfrCount.npz",lfrCount)
        #np.savez("lightFieldRec.npz",lightFieldRec)
    else:
        typ = "plain"
        patch_input = np.zeros((numImgPatches[0]*numImgPatches[1],1,patchSize[0]*patchSize[1]*num*2,1))
        for ky in range(0,numImgPatches[0]):
            if Flags.verbose == 1:
                print 'Processing: %d, %f' % (ky,time.time())
            for kx in range(0,numImgPatches[1]):

                Patch = lightField[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:]
                vecPatch = np.ravel(Patch)

                # Get the phi corresponding to this patch defined by sy and sx
                if Flags.diagPhi == 0:
                    Phi = fm.formPhiSim(patchSizefm,ky*patchSize[0],kx*patchSize[1],1)
                if Flags.normalizePhi == 1:
                    Phi = normalize(Phi)

                # simulate measurement on sensor
                if Flags.interpolate == False:
                    measurement = np.dot(Phi,vecPatch)
                else:
                    measurement = np.dot(Phi.T,np.dot(Phi,vecPatch))
                # subtract mean
                if Flags.subMean == 1:
                    measurement = measurement - np.mean(measurement)

                ## Change image dimension to match the data blob
                patch_input[numImgPatches[1]*ky+kx,:,:,:] = measurement[np.newaxis, np.newaxis, :, np.newaxis]

                if Flags.vverbose == 1:
                    print patch_input.shape

    # Dump the patch_input
    np.savez('patch_input-%d.npz' % (num),patch_input,lightField)
    return 'patch_input-%d.npz' % num

if __name__ == "__main__":
    print 'Dumped in filename: ' + dump_data()
