""" This script
    1 -> reads already compressed light field measurments from real test data
    2 -> do forward pass
    3 -> stitch the output together for reconstruction
"""
# Suren Jayasuriya/Arjun Jauhari
#
# Script to load up a network and test it on a sample lightfield

from lfreconpack import inference
from lfreconpack import formPhi as fm

if __name__ == "__main__":
    net = inference.setup_pycaffe()
    inference.do_inference(inference.load_test_lf_ASP(fm.formPhiSim), net)
