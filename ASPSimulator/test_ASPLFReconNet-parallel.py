""" This script
    1 -> reads test data
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Arjun Jauhari/Suren Jayasuriya
#
# Script to load up a network and test it on a sample lightfield
import sys
import numpy as np
import subprocess
import math
import animateLightField as animLF
import caffe
import calcPSNR
from flags_config import Flags
from flags_config import Config
import time

def do_parallel_recon(filename='patch_input-1.npz'):
    # Print flags
    print "Flags:"
    print "verbose",Flags.verbose
    print "vverbose",Flags.vverbose
    print "animate",Flags.animate
    print "saveanim",Flags.saveanim
    print "overlap",Flags.overlap
    print "normalizePhi",Flags.normalizePhi
    print "subMean",Flags.subMean
    print "diagPhi",Flags.diagPhi

    # Make sure caffe is on the python path
    caffe_root = '../../'
    sys.path.append(caffe_root)
    sys.path.append(caffe_root + 'python')

    #check status quo
    print "Python: ", sys.version.split("\n")[0]
    print "CUDA: ", subprocess.Popen(["nvcc","--version"],stdout=subprocess.PIPE).communicate()[0].split("\n")[3]

    # set paths to model, weights
    Model_file = '../deploy_6fc.prototxt'
    Weights = './sweepResults/BigNet/ASP_Diag_N=1-6fc_iter_100000.caffemodel'

    ## Set up GPU
    caffe.set_device(0)
    caffe.set_device(1)
    caffe.set_mode_gpu()

    #load net
    net = caffe.Net(Model_file,Weights,caffe.TEST)

    # INFO: Blobs are memory abstraction objects (with execution depending on the mode), and data is contained in the field data as an array
    # INFO: net object contains 2 ordered dictionary: blobs and params
    # INFO: net.blobs for input data and its propagation in the layers
    # INFO: net.params a vector of blobs for weight and bias parameters
    if Flags.vverbose == 1:
        print 'Blobs size:'
        print net.inputs
        print [(k, v.data.shape) for k, v in net.blobs.items()]
        print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

    # load test light field
    patchSize = Config.patchSize
    patchSizefm = Config.patchSize[::-1]
    lightFieldRes = Config.lightFieldRes

    numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
    if Flags.verbose == 1:
        print "Number of Patches: ", numImgPatches

    # Load the patch_input
    datanpz = np.load(filename)
    patch_input = datanpz['arr_0']
    lightField = datanpz['arr_1']
    del datanpz

    # Assign it as the network input
    #patch_input = patch_input[0:2600,:,:,:]
    print patch_input.shape
    net.blobs['data'].reshape(*patch_input.shape)
    net.blobs['data'].data[...] = patch_input

    lightFieldRec = np.zeros((1,)+lightFieldRes)

    t1 = time.time()

    ## Call Caffe to Run a forward pass
    net.forward()

    # Reshape output into patchSize
    for ky in range(0,numImgPatches[0]):
        for kx in range(0,numImgPatches[1]):
            lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data[numImgPatches[1]*ky+kx,:,:,:], patchSize)

    t2 = time.time()
    print "Reconstruction time: %f sec" % (t2-t1)

    if Flags.vverbose == 1:
        print 'After:'
        print net.inputs
        print [(k, v.data.shape) for k, v in net.blobs.items()]
        print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

        print 'Output:'
        print net.blobs['r3'].data.shape

    PSNR = calcPSNR.calcPSNR(lightField, lightFieldRec, patchSize)
    print "PSNR: %.2fdB" % (PSNR)
    if Flags.animate == 1 or Flags.saveanim == 1:
        # (TEST) Animate the reconstructed lightfield
        lf = np.transpose(lightFieldRec,(3,4,1,2,0))
        print 'Animating the reconstruction: Close the figure to exit'
        animLF.animateLightField(lf,Weights+'-recon-%.2fdB.mp4' % (PSNR),Flags.saveanim)

    print "DONE"

if __name__ == "__main__":
    do_parallel_recon('patch_input-1.npz')
