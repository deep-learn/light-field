""" This script
    1 -> reads test data
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Suren Jayasuriya
#
# Script to load up a network and test it on a sample lightfield
import sys
import numpy as np
import subprocess
import math
import animateLightField as animLF
import caffe
import calcPSNR
import pdb

# Flags
verbose = 1         # Verbose mode
vverbose = 0        # Extra verbose mode
animate = 1         # Enable animatation of input and output
saveanim = True     # Enable to save the animation
overlap = False     # Reconstruction using overlapping patches
normalizePhi = 1    # To normalizePhi
subMean =1          # Subtract mean from Measurements
diagPhi = 1         # Adding diag flag

# Print flags
print "Flags:"
print "verbose",verbose
print "vverbose",vverbose
print "animate",animate
print "saveanim",saveanim
print "overlap",overlap
print "normalizePhi",normalizePhi
print "subMean",subMean
print "diagPhi",diagPhi

# Make sure caffe is on the python path
caffe_root = '../../'
sys.path.append(caffe_root)
sys.path.append(caffe_root + 'python')

#check status quo
print "Python: ", sys.version.split("\n")[0]
print "CUDA: ", subprocess.Popen(["nvcc","--version"],stdout=subprocess.PIPE).communicate()[0].split("\n")[3]

# set paths to model, weights
#Model_file = '../deploy_11x11x5x5_242.prototxt'
Model_file = '../deploy_11x11x5x5_1452.prototxt'
#Model_file = '../deploy_8x8x5x5.prototxt'
#Weights = '../../../ASPLFrecon_gray_iter_3000.caffemodel'
#Weights = '../../../ASPLFrecon_gray_iter_600.caffemodel'
#Weights = './RealASPLFrecon_gray_sd6_iter_4067.caffemodel'
#Weights = './ASPLFrecon_sd10_normPhi_meansub_iter_8000.caffemodel'
#Weights = './ASPLFRECON_normPhireal_meansub_iter_15000.caffemodel'
#Weights = './ASPLFrecon_sd10_nPhid60_meansub_iter_2000.caffemodel'
#Weights = './ASPLFrecon_sd10_normPhi_meansub_iter_2000.caffemodel'
#Weights = './ASPLFrecon_sd10_nPhid16_meansub_iter_6000.caffemodel'
Weights = './ASP_sims_Diag_N6_iter_15000.caffemodel'
#Weights = './RealASPLFrecon_gray_sd10_d80_iter_10000.caffemodel'
#Weights = './ASPReconNet_iter_15000.caffemodel'

## Set up GPU
caffe.set_device(0)
caffe.set_device(1)
caffe.set_mode_gpu()

#load net
net = caffe.Net(Model_file,Weights,caffe.TEST)

# INFO: Blobs are memory abstraction objects (with execution depending on the mode), and data is contained in the field data as an array
# INFO: net object contains 2 ordered dictionary: blobs and params
# INFO: net.blobs for input data and its propagation in the layers
# INFO: net.params a vector of blobs for weight and bias parameters
if vverbose == 1:
    print 'Blobs size:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

# load test light field
#patchSize = (8,8,5,5)
#patchSizefm = (5, 5, 8, 8)
patchSize = (11,11,5,5)
patchSizefm = (5, 5, 11, 11)
lightFieldRes = (593,840,5,5)

numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
if verbose == 1:
    print "Number of Patches: ", numImgPatches

lightField = np.zeros((1,)+lightFieldRes)
lightFieldRec = np.zeros((1,)+lightFieldRes)
lfrMoveValue = np.zeros((1,)+lightFieldRes)
lfrCount = np.zeros((1,)+lightFieldRes)

# Load the patch_input
temp = np.load('patch_input-6.npz')
patch_input = temp['arr_0']

patch_input = patch_input[0:2600,:,:,:]
print patch_input
net.blobs['data'].reshape(*patch_input.shape)
net.blobs['data'].data[...] = patch_input

pdb.set_trace()
## Call Caffe to Run a forward pass
net.forward()

# Reshape output into patchSize
for ky in range(0,numImgPatches[0]):
    if verbose == 1:
        print 'Processing: ', (ky)
    for kx in range(0,numImgPatches[1]):
        #lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data[numImgPatches[1]*ky+kx,:,:,:], patchSize)
        if numImgPatches[1]*ky+kx < 2600:
            lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data[numImgPatches[1]*ky+kx,:,:,:], patchSize)
#lightFieldRec[0,:,:,:,:] = np.reshape(net.blobs['r3'].data, patchSize)
if vverbose == 1:
    print 'After:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

    print 'Output:'
    print net.blobs['r3'].data.shape

PSNR = calcPSNR.calcPSNR(lightField, lightFieldRec, patchSize)
print "PSNR: %.2fdB" % (PSNR)
if animate == 1 or saveanim == 1:
    # (TEST) Animate the reconstructed lightfield
    lf = np.transpose(lightFieldRec,(3,4,1,2,0))
    print 'Animating the reconstruction: Close the figure to exit'
    animLF.animateLightField(lf,Weights+'-recon-%.2fdB.mp4' % (PSNR),saveanim)

print "DONE"

""" This script
    1 -> reads test data
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Suren Jayasuriya
#
# Script to load up a network and test it on a sample lightfield
import sys
import numpy as np
import scipy
import subprocess
import math
import cv2
import os
import animateLightField as animLF
import caffe
from os.path import expanduser
import formPhi as fm
import calcPSNR
from normalize import normalize
import pdb

# Flags
verbose = 1         # Verbose mode
vverbose = 0        # Extra verbose mode
animate = 1         # Enable animatation of input and output
saveanim = True     # Enable to save the animation
overlap = False     # Reconstruction using overlapping patches
normalizePhi = 1    # To normalizePhi
subMean =1          # Subtract mean from Measurements
diagPhi = 1         # Adding diag flag

# Print flags
print "Flags:"
print "verbose",verbose
print "vverbose",vverbose
print "animate",animate
print "saveanim",saveanim
print "overlap",overlap
print "normalizePhi",normalizePhi
print "subMean",subMean
print "diagPhi",diagPhi

# Make sure caffe is on the python path
caffe_root = '../../'
sys.path.append(caffe_root)
sys.path.append(caffe_root + 'python')

#check status quo
print "Python: ", sys.version.split("\n")[0]
print "CUDA: ", subprocess.Popen(["nvcc","--version"],stdout=subprocess.PIPE).communicate()[0].split("\n")[3]

# set paths to model, weights
#Model_file = '../deploy_11x11x5x5_242.prototxt'
Model_file = '../deploy_11x11x5x5_1452.prototxt'
#Model_file = '../deploy_8x8x5x5.prototxt'
#Weights = '../../../ASPLFrecon_gray_iter_3000.caffemodel'
#Weights = '../../../ASPLFrecon_gray_iter_600.caffemodel'
#Weights = './RealASPLFrecon_gray_sd6_iter_4067.caffemodel'
#Weights = './ASPLFrecon_sd10_normPhi_meansub_iter_8000.caffemodel'
#Weights = './ASPLFRECON_normPhireal_meansub_iter_15000.caffemodel'
#Weights = './ASPLFrecon_sd10_nPhid60_meansub_iter_2000.caffemodel'
#Weights = './ASPLFrecon_sd10_normPhi_meansub_iter_2000.caffemodel'
#Weights = './ASPLFrecon_sd10_nPhid16_meansub_iter_6000.caffemodel'
Weights = './ASP_sims_Diag_N6_iter_15000.caffemodel'
#Weights = './RealASPLFrecon_gray_sd10_d80_iter_10000.caffemodel'
#Weights = './ASPReconNet_iter_15000.caffemodel'

## Set up GPU
caffe.set_device(0)
caffe.set_mode_gpu()

#load net
net = caffe.Net(Model_file,Weights,caffe.TEST)

# INFO: Blobs are memory abstraction objects (with execution depending on the mode), and data is contained in the field data as an array
# INFO: net object contains 2 ordered dictionary: blobs and params
# INFO: net.blobs for input data and its propagation in the layers
# INFO: net.params a vector of blobs for weight and bias parameters
if vverbose == 1:
    print 'Blobs size:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

# load test light field
#patchSize = (8,8,5,5)
#patchSizefm = (5, 5, 8, 8)
patchSize = (11,11,5,5)
patchSizefm = (5, 5, 11, 11)
lightFieldRes = (593,840,5,5)

numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
if verbose == 1:
    print "Number of Patches: ", numImgPatches

lightField = np.zeros((1,)+lightFieldRes)
lightFieldRec = np.zeros((1,)+lightFieldRes)
lfrMoveValue = np.zeros((1,)+lightFieldRes)
lfrCount = np.zeros((1,)+lightFieldRes)

Path = os.path.dirname(os.path.realpath(__file__)) + '/../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'
# Load all the images and normalize them
for ky in range(0,5):
    for kx in range(0,5):
        # read current image
        I = cv2.imread(Path + '../DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
        I = I.astype('float64')
        I = I - I.min()
        I = I/(I.max()-I.min())

        lightField[0,:,:,ky,kx] = I

# (TEST) Animated the loaded lightfield
if animate == 1 or saveanim == 1:
    lf = np.transpose(lightField,(3,4,1,2,0))
    print 'Animating the input: Close the figure to proceed'
    animLF.animateLightField(lf,'ASPLFOriginal.mp4',saveanim)

############################################################
# Preparing block diagonal Phi
############################################################
if diagPhi == 1:
    patchsizeDiag = (5, 5, 1, 1)
    num = 6
    phiBlock = np.zeros((2*num,patchsizeDiag[0]*patchsizeDiag[1]))
    for i in xrange(num):
        phiBlock[2*i:2*i+2,:] = fm.formPhi(patchsizeDiag,i+1,i+1,1)

    #print phiBlock.shape
    #hinton.hinton(phiBlock,"phiBlock")
    #hinton.hinton(normalize(phiBlock),"phiBlock")
    #print normalize(phiBlock)

    # Make block diagonal
    Phi = np.kron(np.eye(patchSize[0]*patchSize[1]), phiBlock)
    #print Phi.shape
    #hinton.hinton(Phi,"phiBlock")

############################################################

# Create patches, send it through the network, and place it in reconstructed light field
if overlap == True:
    typ = "overlap"
    # Create Overlapping patches
    for ky in range(0,lightFieldRes[0]-patchSize[0],4):
        for kx in range(0,lightFieldRes[1]-patchSize[1],4):
            if verbose == 1:
                print 'Processing: ', (ky,kx)

            Patch = lightField[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:]
            vecPatch = np.ravel(Patch)

            # Get the phi corresponding to this patch defined by sy and sx
            if diagPhi == 0:
                Phi = fm.formPhi(patchSizefm,ky,kx,1)
            if normalizePhi == 1:
                Phi = normalize(Phi)

            # simulate measurement on sensor
            measurement = np.dot(Phi,vecPatch)
            # subtract mean
            if subMean == 1:
                measurement = measurement - np.mean(measurement)

            ## Change image dimension to match the data blob
            patch_input = measurement[np.newaxis, np.newaxis, :, np.newaxis]
            if vverbose == 1:
                print "patch_input",patch_input.shape
            net.blobs['data'].reshape(*patch_input.shape)
            net.blobs['data'].data[...] = patch_input

            ## Call Caffe to Run a forward pass
            net.forward()

            # Initialize variables for intermediate values
            lfrMoveValuetmp = np.zeros((1,)+lightFieldRes)
            lfrCounttmp = np.zeros((1,)+lightFieldRes)

            # Reshape output into patchSize
            lfrMoveValuetmp[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] = np.reshape(net.blobs['r3'].data, patchSize)
            lfrCounttmp[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] = np.ones(patchSize,dtype=np.float64)
            lfrMoveValue += lfrMoveValuetmp
            lfrCount += lfrCounttmp

    # Normalize
    lightFieldRec = lfrMoveValue/lfrCount
    # Setting the nans to 0
    lightFieldRec[np.isnan(lightFieldRec)] = 0
    #np.savez("lfrMoveValue.npz",lfrMoveValue)
    #np.savez("lfrCount.npz",lfrCount)
    #np.savez("lightFieldRec.npz",lightFieldRec)
else:
    typ = "plain"
    # FIXME: Phi might not be defined if using diagPhi = 0
    patch_input = np.zeros((numImgPatches[0]*numImgPatches[1],1,Phi.shape[0],1))
    for ky in range(0,numImgPatches[0]):
        if verbose == 1:
            print 'Processing: ', (ky)
        for kx in range(0,numImgPatches[1]):
            #if verbose == 1:
            #    print 'Processing: ', (ky,kx)

            Patch = lightField[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:]
            vecPatch = np.ravel(Patch)

            # Get the phi corresponding to this patch defined by sy and sx
            if diagPhi == 0:
                Phi = fm.formPhi(patchSizefm,ky*patchSize[0],kx*patchSize[1],1)
            if normalizePhi == 1:
                Phi = normalize(Phi)

            # simulate measurement on sensor
            measurement = np.dot(Phi,vecPatch)
            # subtract mean
            if subMean == 1:
                measurement = measurement - np.mean(measurement)

            ## Change image dimension to match the data blob
            patch_input[numImgPatches[1]*ky+kx,:,:,:] = measurement[np.newaxis, np.newaxis, :, np.newaxis]
            #patch_input = measurement[np.newaxis, np.newaxis, :, np.newaxis]
            if vverbose == 1:
                print patch_input.shape
            #net.blobs['data'].reshape(*patch_input.shape)
            #net.blobs['data'].data[...] = patch_input

            ### Call Caffe to Run a forward pass
            #net.forward()

            ## Reshape output into patchSize
            #lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data, patchSize)

# Dump the patch_input
np.savez('patch_input-%d.npz' % num,patch_input)
#patch_input = patch_input[0:2014,:,:,:]
net.blobs['data'].reshape(*patch_input.shape)
net.blobs['data'].data[...] = patch_input

pdb.set_trace()
## Call Caffe to Run a forward pass
net.forward()

# Reshape output into patchSize
#lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data, patchSize)
lightFieldRec[0,:,:,:,:] = np.reshape(net.blobs['r3'].data, patchSize)
if vverbose == 1:
    print 'After:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

    print 'Output:'
    print net.blobs['r3'].data.shape

PSNR = calcPSNR.calcPSNR(lightField, lightFieldRec, patchSize)
print "PSNR: %.2fdB" % (PSNR)
if animate == 1 or saveanim == 1:
    # (TEST) Animate the reconstructed lightfield
    lf = np.transpose(lightFieldRec,(3,4,1,2,0))
    print 'Animating the reconstruction: Close the figure to exit'
    animLF.animateLightField(lf,Weights+'%s-recon-%.2fdB.mp4' % (typ,PSNR),saveanim)

print "DONE"
