import numpy as np
import h5py
import os

def weighted_loss(patch,patchSize):
    weightMat = np.sqrt(np.array([5,4,3,4,5,
                                  4,3,2,3,4,
                                  3,2,1,2,3,
                                  4,3,2,3,4,
                                  5,4,3,4,5]))
    #weightMat = np.sqrt(np.ones(patchSize[2]*patchSize[3]))
    weightMask = np.tile(weightMat,patchSize[0]*patchSize[1])

    return patch*weightMask

def dump_weight_h5(patchSize):
    weightMat = np.sqrt(np.array([5,4,3,4,5,
                                  4,3,2,3,4,
                                  3,2,1,2,3,
                                  4,3,2,3,4,
                                  5,4,3,4,5]))
    #weightMat = np.sqrt(np.ones(patchSize[2]*patchSize[3]))
    weightMask = np.tile(weightMat,patchSize[0]*patchSize[1])

    # Initialize weight
    weight = np.zeros((1,1,np.prod(patchSize)),dtype='float64')
    sample = np.zeros((1,1,1),dtype='float64')

    weight[0,0,:] = weightMask
    sample[0,0,:] = 1

    #create hdf5 formats compatible with Caffe
    wfn = "weightl2.h5"
    f  = h5py.File(wfn,"w")
    f['weight'] = weight.astype('float64')
    f['sample'] = sample.astype('float64')
    f.close()
    print "Dumped: %s at %s" % (wfn,os.getcwd())

