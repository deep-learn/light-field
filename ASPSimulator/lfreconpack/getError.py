import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def l1loss(a,b):
    err = a - b
    return np.sum(abs(err))

def l2loss(a,b):
    err = a - b
    return np.sqrt(np.sum(np.square(err)))

def errorMap(a,b,name):
    err = np.abs(a-b)
    plt.imshow(err,cmap='Greys_r')
    plt.axis('off')
    plt.savefig(name, bbox_inches='tight', pad_inches=0.0)

orig = np.load("dice-orig.npz")
reco = np.load("dice-ASP_Diag_N=1-6fc_iter_100000.caffemodelplain-recon-25.66dB.mp4.npz")

## x and y axis
xdim = (orig['arr_0'].shape[1]/11)*11
ydim = (orig['arr_0'].shape[2]/11)*11
udim = orig['arr_0'].shape[3]
vdim = orig['arr_0'].shape[4]
l1error = np.zeros((udim,vdim))
l2error = np.zeros((udim,vdim))
for i in xrange(udim):
    for j in xrange(vdim):
        l1error[i,j] = l1loss(orig['arr_0'][0,0:xdim,0:ydim,i,j], reco['arr_0'][0,0:xdim,0:ydim,i,j])
        l2error[i,j] = l2loss(orig['arr_0'][0,0:xdim,0:ydim,i,j], reco['arr_0'][0,0:xdim,0:ydim,i,j])
        errorMap(orig['arr_0'][0,0:xdim,0:ydim,i,j], reco['arr_0'][0,0:xdim,0:ydim,i,j],'errMap-%d%d' % (i,j))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x = np.arange(1,udim+1)
y = np.arange(1,vdim+1)
X, Y = np.meshgrid(x, y)

ax.plot_surface(X, Y, l2error,
        rstride=1,
        cstride=1,
        cmap=cm.RdPu,
        linewidth=1,
        antialiased=True)

ax.set_title('l2Error across various Angle Views')        # title
ax.set_xlabel('U direction')
ax.set_ylabel('V direction')
ax.set_zlabel('l2 Error')
ax.view_init(elev=30,azim=70)                # elevation & angle
#ax.dist=8                                    # distance from the plot
plt.show()

print l1error
print l2error
