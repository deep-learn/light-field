import numpy as np
import Gw_sj

def ASPProjMatrixTwoPhase(patchSize,sy,sx):
    """ Input:
        patchSize: [angulary,angularx,spatialy,spatialx]
        sy: y pos of top left corner of patch
        sx: x pos of top left corner of patch
    """
    # Load ASP paramaters
    #wlow = 1
    #wmid = 0.2
    #whigh = 0.1
    #wDC = 0.005

    wlow = 1
    wmid = 1
    whigh = 1
    wDC = 1
    # Create w array 1x(48+1)
    w = np.full(16,wlow)
    w = np.concatenate((w,np.full(16,wmid)))
    w = np.concatenate((w,np.full(16,whigh)))
    w = np.append(w,wDC)

    b_low = 12
    b_mid = 18
    b_high = 24

    par = np.empty([49,3])

    par[0,:]=   [b_low,0,0];
    par[1,:]=   [b_low,0,180];
    par[2,:]=   [b_low,0,270];
    par[3,:]=   [b_low,0,90];

    par[4,:]=   [b_low,90,270];
    par[5,:]=   [b_low,90,90];
    par[6,:]=   [b_low,90,180];
    par[7,:]=   [b_low,90,0];

    par[8,:]=   [b_low,45,270];
    par[9,:]=   [b_low,45,90];
    par[10,:]=  [b_low,45,180];
    par[11,:]=  [b_low,45,0];

    par[12,:]=  [b_low,-45,270];
    par[13,:]=  [b_low,-45,90];
    par[14,:]=  [b_low,-45,180];
    par[15,:]=  [b_low,-45,0];

    par[16,:]=  [b_mid,22.5,180];
    par[17,:]=  [b_mid,22.5,0];
    par[18,:]=  [b_mid,22.5,270];
    par[19,:]=  [b_mid,22.5,90];

    par[20,:]=  [b_mid,-67.5,270];
    par[21,:]=  [b_mid,-67.5,90];
    par[22,:]=  [b_mid,-67.5,180];
    par[23,:]=  [b_mid,-67.5,0];

    par[24,:]=  [b_mid,-22.5,180];
    par[25,:]=  [b_mid,-22.5,0];
    par[26,:]=  [b_mid,-22.5,270];
    par[27,:]=  [b_mid,-22.5,90];

    par[28,:]=  [b_mid,67.5,180];
    par[29,:]=  [b_mid,67.5,0];
    par[30,:]=  [b_mid,67.5,270];
    par[31,:]=  [b_mid,67.5,90];

    par[32,:]=  [b_high,0,270];
    par[33,:]=  [b_high,0,90];
    par[34,:]=  [b_high,0,180];
    par[35,:]=  [b_high,0,0];

    par[36,:]=  [b_high,90,180];
    par[37,:]=  [b_high,90,0];
    par[38,:]=  [b_high,90,270];
    par[39,:]=  [b_high,90,90];

    par[40,:]=  [b_high,45,180];
    par[41,:]=  [b_high,45,0];
    par[42,:]=  [b_high,45,270];
    par[43,:]=  [b_high,45,90];

    par[44,:]=  [b_high,-45,180];
    par[45,:]=  [b_high,-45,0];
    par[46,:]=  [b_high,-45,270];
    par[47,:]=  [b_high,-45,90];

    par[48,:]=  [0,0,0];

    nfSort1=np.array([[44, 40,  1, 48, 12, 22],
                      [ 4, 42,  8, 28, 34, 32],
                      [26, 24, 16, 20, 10, 38],
                      [36, 46, 30,  6, 18, 14]])

    nfSort2=np.array([[43, 39,  2, 47, 11, 21],
                      [ 3, 41,  7, 27, 33, 31],
                      [25, 23, 15, 19,  9, 37],
                      [35, 45, 29,  5, 17, 13]])

    nfSort1 = nfSort1-1
    nfSort2 = nfSort2-1

    extSort1 = np.tile(nfSort1,(np.ceil(patchSize[2]/float(nfSort1.shape[0]))+1,np.ceil(patchSize[3]/float(nfSort1.shape[1]))+1));
    sortoffsety = np.mod(sy,nfSort1.shape[0])
    sortoffsetx = np.mod(sx,nfSort1.shape[1])
    shiftSort1 = extSort1[sortoffsety:sortoffsety+patchSize[2],sortoffsetx:sortoffsetx+patchSize[3]]

    extSort2 = np.tile(nfSort2,(np.ceil(patchSize[2]/float(nfSort2.shape[0]))+1,np.ceil(patchSize[3]/float(nfSort2.shape[1]))+1));
    shiftSort2 = extSort2[sortoffsety:sortoffsety+patchSize[2],sortoffsetx:sortoffsetx+patchSize[3]];

    #print shiftSort1.shape
    #print shiftSort2.shape

    M1 = np.zeros(patchSize);
    M2 = np.zeros(patchSize);

    M = patchSize[0];   # n samples in response (long axis)
    A = 0.5; # peak amplitude of response
    S = 0.7; # scale of gaussian window

    # Compute DC value
    g = w[48]*Gw_sj.Gw_sj(M,A,par[48,:],S);

    for spy in range(patchSize[2]):
        for spx in range(patchSize[3]):
            nASP = shiftSort1[spy,spx];
            M1[:,:,spy,spx] = g + w[nASP]*Gw_sj.Gw_sj(M,A,par[nASP,:],S);

    for spy in range(patchSize[2]):
        for spx in range(patchSize[3]):
            nASP = shiftSort2[spy,spx];
            M2[:,:,spy,spx] = g + w[nASP]*Gw_sj.Gw_sj(M,A,par[nASP,:],S);

    return [M1,M2, (sortoffsety, sortoffsetx)]
