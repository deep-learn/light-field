""" This module implements functions used during test/inference """

import sys
import numpy as np
import scipy
import subprocess
import math
import cv2
import os
import caffe
import time
import re
from os.path import expanduser
import matplotlib.pyplot as plt
import scipy.io as sio

import formPhi as fm
import calcPSNR
import normalize
import animateLightField as animLF
from flags_config import Flags
from flags_config import Config
from get_training_data import _patch_to_measurement, load_lf_to_tensor, _measurement_with_loc


def setup_pycaffe():
    # Make sure caffe is on the python path
    caffe_root = '../../'
    sys.path.append(caffe_root)
    sys.path.append(caffe_root + 'python')

    # check status quo
    print "Python: ", sys.version.split("\n")[0]
    print "CUDA: ", subprocess.Popen(["nvcc","--version"],stdout=subprocess.PIPE).communicate()[0].split("\n")[3]

    # Set up GPU
    caffe.set_device(0)
    caffe.set_mode_gpu()

    # load net
    net = caffe.Net(Config.Model_file,Config.Weights,caffe.TEST)

    # INFO: Blobs are memory abstraction objects (with execution depending on the mode), and data is contained in the field data as an array
    # INFO: net object contains 2 ordered dictionary: blobs and params
    # INFO: net.blobs for input data and its propagation in the layers
    # INFO: net.params a vector of blobs for weight and bias parameters
    if Flags.vverbose == 1:
        print 'Blobs size:'
        print net.inputs
        print [(k, v.data.shape) for k, v in net.blobs.items()]
        print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

    return net


def load_test_lf_syn(formPhi_func):
    patchSize = Config.patchSize
    lightFieldRes = Config.lightFieldRes

    Path = os.path.dirname(os.path.realpath(__file__)) + '/../../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'
    trainingLightFieldFilenames = ['fishi_5x5_ap35','messerschmitt_5x5_ap35','shrubbery_5x5_ap35', 'dice_5x5_ap35', 'dragons_bunnies_5x5_ap6.6']
    trainingLFnames = ['fishi-','messerschmitt-','shrubbery-', 'dice-', 'dragons-']

    LFnum = 4
    lightField = load_lf_to_tensor(Path, [trainingLightFieldFilenames[LFnum]])[0][0]

    # Dump light-field and light-field Rec for Denoise NET in form of 2D images
    np.savez(trainingLFnames[LFnum]+'orig',lightField)

    numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
    if Flags.verbose == 1:
        print "Number of Patches: ", numImgPatches

    num = Config.diagPhi_num if Flags.diagPhi == 1 else 1

    patch_input = []
    # Create patches, which can be sent through the network
    for ky in range(0, lightFieldRes[0], Config.stridey):
        for kx in range(0, lightFieldRes[1], Config.stridex):
            Patch = lightField[0, ky:ky+patchSize[0], kx:kx+patchSize[1], :, :]
            if np.prod(Patch.shape) < np.prod(patchSize):   # for border
                continue
            vecPatch = np.ravel(Patch)

            measurement, _, _ = _patch_to_measurement(vecPatch, formPhi_func, patchSize, ky, kx, num)

            ## Change image dimension to match the data blob
            patch_input.append((measurement[np.newaxis, np.newaxis, :, np.newaxis], (ky, kx)))

    print "Loaded test data"
    return [patch_input, trainingLFnames[LFnum], (lightField, lightFieldRes)]


def load_test_lf_ASP(formPhi_func):
    patchSize = Config.patchSize

    # Load all the sensor measurements
    datapath = os.path.dirname(os.path.realpath(__file__)) + '/../../ASP Reconstruction Pipeline/DATA/Datasets/PigDataset/'
    filename = 'White_ktep_firstscene_p6.mat'
    sensorData = sio.loadmat(datapath+filename)
    lightFieldRes = sensorData['im1'].shape+(5,5)

    numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
    if Flags.verbose == 1:
        print "Number of Patches: ", numImgPatches

    # Mode 1=Differential 0=Dual
    bRecoverDiff=1
    print "Differential mode" if bRecoverDiff == 1 else "Common Mode"

    patch_input = []
    for ky in range(0, lightFieldRes[0], Config.stridey):
        for kx in range(0, lightFieldRes[1], Config.stridex):
            # read measurement corresponding to current patch on sensor and vectorize it shape should be (2*patchSize[0]*patchSize[1],1)
            measurement1 = np.ravel(np.rot90(np.fliplr(sensorData['im1'][ky:ky+patchSize[0],kx:kx+patchSize[1]])), order='F')
            measurement2 = np.ravel(np.rot90(np.fliplr(sensorData['im2'][ky:ky+patchSize[0],kx:kx+patchSize[1]])), order='F')
            if bRecoverDiff == 1:
                temp1 = measurement2-measurement1
                temp2 = (measurement2+measurement1)/8.0
                measurement  = np.concatenate((temp1[:,np.newaxis],temp2[:,np.newaxis]))
            else:
                measurement  = np.concatenate((measurement1[:,np.newaxis],measurement2[:,np.newaxis]))

            measurementSize = patchSize[0]*patchSize[1]*2
            if np.prod(measurement.shape) < measurementSize:   # for border
                continue

            # simulate measurement on sensor
            if Flags.interpolate:
                Phi, loc = formPhi_func(patchSize,ky,kx,1)
                measurement = np.dot(Phi.T, measurement)

            # subtract mean
            if Flags.subMean == 1:
                measurement = measurement - np.mean(measurement,0)

            if Flags.patch_loc:
                measurement, _ = _measurement_with_loc(measurement, ky, kx)

            measurement = 500*(measurement - 0.01)      # magic scaling
            #print measurement.max(), min(measurement), np.mean(measurement)
            ## Change image dimension to match the data blob
            patch_input.append((measurement[np.newaxis, np.newaxis, :, :], (ky, kx)))

    lightField = np.zeros((1,)+lightFieldRes)   # only needed for consistency
    print "Loaded ASP test data"
    return [patch_input, filename, (lightField, lightFieldRes)]


# NOTE: In this function output layer of the network is hardcoded to r2, may change if the deploy file changes
def do_inference(data_in, net):
    # unpack data_in
    patch_input = data_in[0]
    lfname = data_in[1]
    lightField = data_in[2][0]
    lightFieldRes = data_in[2][1]

    patchSize = Config.patchSize

    lightFieldRec = np.zeros((1,)+lightFieldRes)
    lfrMoveValue = np.zeros((1,)+lightFieldRes)
    lfrCount = np.zeros((1,)+lightFieldRes)

    if Flags.verbose == 1:
        print "Number of Patches: ", len(patch_input)

    if Flags.verbose == 1:
        print "patch_input shape",patch_input[0][0].shape

    prev_ky = -1
    st_time = time.time()
    for patch, loc in patch_input:
        net.blobs['data'].reshape(*patch.shape)
        net.blobs['data'].data[...] = patch

        ## Call Caffe to Run a forward pass
        net.forward()

        ky = loc[0]
        kx = loc[1]
        if Flags.verbose == 1 and prev_ky != ky:
            prev_ky = ky
            print 'Processing: %d, %f' % (ky,time.time())

        # Reshape output into patchSize
        lfrMoveValue[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] += np.reshape(net.blobs['r2'].data, patchSize)
        lfrCount[0,ky:ky+patchSize[0],kx:kx+patchSize[1],:,:] += np.ones(patchSize,dtype=np.float64)

    # Average out
    lightFieldRec = lfrMoveValue/lfrCount
    lightFieldRec[np.isnan(lightFieldRec)] = 0  # Setting the nans to 0

    recon_time = time.time() - st_time      # total recon time

    if Flags.vverbose == 1:
        print 'After:'
        print net.inputs
        print [(k, v.data.shape) for k, v in net.blobs.items()]
        print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

        print 'Output:'
        print net.blobs['r2'].data.shape

    PSNR = calcPSNR.calcPSNR(lightField, lightFieldRec, patchSize)
    print "PSNR: %.2fdB" % (PSNR)
    fname_ext = lfname+'-stride'+str(Config.stridex)+'-rec_time'+str(int(recon_time))+'-%.2fdB.mp4' % (PSNR)
    if Flags.animate == 1 or Flags.saveanim == 1:
        # (TEST) Animate the reconstructed lightfield
        lf = np.transpose(lightFieldRec,(3,4,1,2,0))
        print 'Animating the reconstruction: Close the figure to exit'
        animLF.animateLightField(lf,Config.Weights+fname_ext,Flags.saveanim)

    # Dump light-field and light-field Rec for Denoise NET in form of 2D images
    np.savez(lfname + re.search(r'(.*)\.caffemodel',os.path.basename(Config.Weights)).group(1)+fname_ext,lightFieldRec)
    print "INFERENCE DONE"
