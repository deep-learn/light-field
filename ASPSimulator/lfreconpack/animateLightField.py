## Arjun Jauhari
##
## Show a wiggling light field.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.animation as animation

def animateLightField(lightField, filename=None, saveanim=False):

    # expected [angle_y angle_x spatial_y spatial_x numColorChannels]
    lightFieldResolution = lightField.shape;

    indices = [(iy, ix) for iy in range(lightFieldResolution[0]) for ix in range(lightFieldResolution[1])] +\
              [(iy, ix) for ix in range(lightFieldResolution[1]) for iy in range(lightFieldResolution[0])]

    numColorChannels = 1;
    if np.size(lightFieldResolution) > 4:
        numColorChannels = lightFieldResolution[4];

    frameCount = 1;
    pauseTime = 0.2;

    ## create figure object
    fig = plt.figure()

    ## create animation
    anim = animation.FuncAnimation(fig, animate, fargs=(indices,lightFieldResolution,lightField,numColorChannels), frames=len(indices), interval=pauseTime*1000, blit=True, repeat=True);

    plt.axis('off')

    # Save the animation
    if saveanim == True:
        anim.save(filename, fps=3, writer='mencoder')
    plt.show()

## Update function
def animate(i,indices,lightFieldResolution,lightField,numColorChannels):
    I = np.reshape(lightField[indices[i][0],indices[i][1],:,:,:], (lightFieldResolution[2], lightFieldResolution[3], numColorChannels));
    if numColorChannels == 1:
        return [plt.imshow(I.squeeze(), cmap="Greys_r")]; # make it iterable and return
    else:
        return [plt.imshow(I.squeeze())]; # make it iterable and return

# Test
if __name__ == "__main__":
    # load original light field
    # number of pixels in [angle_y angle_x spatial_y spatial_x numColorChannels]
    lightFieldResolution = (5, 5, 593, 840, 3);
    lightField = np.zeros(lightFieldResolution);

    for ky in range(lightFieldResolution[0]):
        for kx in range(lightFieldResolution[1]):
            #I = mpimg.imread('/home/molnargroup/Documents/CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldResolution[1]+kx+1) + '.png');
            I = mpimg.imread('/home/arjun/Desktop/Cornell_courses/deeplearning/CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldResolution[1]+kx+1) + '.png');
            lightField[ky,kx,:,:,:] = np.reshape(I, (1, 1, lightFieldResolution[2], lightFieldResolution[3], 3));
    animateLightField(lightField,1)
