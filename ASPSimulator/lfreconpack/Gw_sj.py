# Suren Jayasuriya
#
# Gabor Wavelet generator

import numpy as np
import matplotlib.pyplot as plt

# Function defn for Gabor Wavelet: N = grid size, m = modulation parameter, ASPprms = [beta, chi, alpha]
def Gw_sj(N,m,ASPprms,sc=1):
    # Standard deviation
    sigx = sc*np.pi/15
    sigy = sc*np.pi/15

    # ASP parameters
    beta = ASPprms[0]
    chi = ASPprms[1]*np.pi/180 # in radians
    alpha = ASPprms[2]*np.pi/180 # in radians

    # create grid in 2D angle space
    t = np.linspace(-np.pi/10,np.pi/10,N)
    p = np.linspace(-np.pi/10,np.pi/10,N)
    tt,pp = np.meshgrid(t,p)

    T = tt*np.cos(chi)+pp*np.sin(chi)
    P = -1*tt*np.sin(chi) + pp*np.cos(chi)

    A = A_sj(T,P,sigx,sigy)

    out = A*2*m*np.cos(beta*T+alpha)

    # plotting the function
    #plt.imshow(out.squeeze(),'Greys_r');
    #plt.show();

    return out


def A_sj(T,P,sigx,sigy):
    return 1/(2*np.pi*sigx*sigy)*np.exp(-0.5*np.square(T/sigx) + -0.5*np.square(P/sigy))
