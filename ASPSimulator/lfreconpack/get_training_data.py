""" This module implements functions used to generate training data """

import re
import cv2
import numpy as np
import os
import h5py
import sys

from normalize import normalize
import hinton
import weighted_loss as wl
import animateLightField as animlf
from flags_config import Flags
from flags_config import Config


def lytroimg_to_lf(imgpath, view=(4, 12)):
    numimg_sx = 14
    numimg_sy = 14
    input_img = cv2.imread(imgpath)

    h = input_img.shape[0]/numimg_sy
    w = input_img.shape[1]/numimg_sx
    n_angular = view[1] - view[0]
    full_lf = np.zeros((h, w, 3, n_angular, n_angular))

    dump_dir = imgpath[:-4]
    fn_prefix = re.search('(.+?)_', os.path.basename(imgpath)).group(1)

    for x, ax in enumerate(range(view[0], view[1])):
        for y, ay in enumerate(range(view[0], view[1])):
            full_lf[:, :, :, y, x] =\
                input_img[ay::numimg_sy, ax::numimg_sx, :]
            if not os.path.exists(dump_dir):
                os.makedirs(dump_dir)
            cv2.imwrite(os.path.join(dump_dir, fn_prefix + '-%02d' % (y*n_angular+x+1) + '.png'), full_lf[:, :, :, y, x])

    print 'LF dimension: ', full_lf.shape
    return full_lf


def load_lf_to_tensor(Path=None, trainingLightFieldFilenames=None):
    if trainingLightFieldFilenames is None:
        trainingLightFieldFilenames = ['fishi_5x5_ap35', 'messerschmitt_5x5_ap35', 'shrubbery_5x5_ap35', 'dice_5x5_ap35']

    # number of training datasets
    numTrainingLightFields = len(trainingLightFieldFilenames)

    lightFieldRes = Config.lightFieldRes

    # Preallocate TT
    TT = np.zeros((numTrainingLightFields, 1)+lightFieldRes)

    # load all 4D training light fields
    if Path is None:
        Path = os.path.dirname(os.path.realpath(__file__)) + '/../../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'

    for k in range(0, numTrainingLightFields):
        shortname = re.search('(.+?)_', os.path.basename(trainingLightFieldFilenames[k])).group(1)
        print "Loading: ", shortname

        for ky in range(lightFieldRes[2]):
            for kx in range(lightFieldRes[3]):

                # Read image in grayscale
                I = cv2.imread(Path + trainingLightFieldFilenames[k] + '/' + shortname + '-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png', 0)
                I = I.astype('float64')
                I = I-I.min()
                I = I/(I.max()-I.min())

                # Store values, [LF_indx,nColor,spatial_y,spatial_x,angular_y,angular_x]
                pady = lightFieldRes[0] - I.shape[0]    # this is to take care
                padx = lightFieldRes[1] - I.shape[1]    # resolution mismatch of LF in UCSD data
                TT[k, 0, :, :, ky, kx] = np.pad(I, ((0, pady), (0, padx)), 'constant', constant_values=0)
    # animate
    if Flags.animate == 1:
        # Changing the shape of TT to expected animated shape
        # expected [angular_y angular_x spatial_y spatial_x numColorChannels]
        # Just animating the first(fishy) data
        lf = np.transpose(TT[0,:,:,:,:,:],(3,4,1,2,0))
        print 'Animating the input: Close the figure to proceed'
        animlf.animateLightField(lf)
    return [TT, numTrainingLightFields]


def _diag_phi(num, formPhi_func, patchSize):
    ############################################################
    # Preparing block diagonal Phi
    ############################################################
    patchsizeDiag = (1, 1) + patchSize[2:4]
    phiBlock = np.zeros((2*num,patchsizeDiag[2]*patchsizeDiag[3]))
    for i in xrange(num):
        yi = i/num    # since both are integers, divide = floor
        xi = np.mod(i,num)
        phiBlock[2*i:2*i+2,:], loc = formPhi_func(patchsizeDiag,yi,xi,1)

    #print phiBlock.shape
    #hinton.hinton(phiBlock,"phiBlock")
    #hinton.hinton(normalize(phiBlock),"phiBlock")
    #print normalize(phiBlock)

    # Make block diagonal
    Phi = np.kron(np.eye(patchSize[0]*patchSize[1]), phiBlock)
    #print Phi.shape
    #hinton.hinton(Phi,"phiBlock")

    return Phi, loc


def _get_loc_dummy(y, x):
    n_y = 4     # tile height
    n_x = 6     # tile width
    sortoffsety = np.mod(y, n_y)
    sortoffsetx = np.mod(x, n_x)

    return (sortoffsety, sortoffsetx)

def _measurement_with_loc(measurement, y, x):
    # Get the loc corresponding to this patch defined by y and x
    loc = _get_loc_dummy(y, x)
    print 'aj debug', measurement.shape
    M_orig = measurement.shape[0]
    M = M_orig*24
    pos = _get_pos(loc, M_orig)
    temp = np.zeros((M,1))
    temp[pos:pos+M_orig,:] = measurement
    measurement = temp

    return measurement, loc


def _patch_to_measurement(vecPatch, formPhi_func, patchSize, y, x, num):
    # Get the phi corresponding to this patch defined by y and x
    Phi, loc = formPhi_func(patchSize,y,x,1) if Flags.diagPhi == 0 else _diag_phi(num)

    if Flags.normalizePhi == 1:
        Phi = normalize(Phi)

    # simulate measurement on sensor
    if Flags.interpolate == False:
        Measurements = np.dot(Phi,vecPatch)
    else:
        Measurements = np.dot(Phi.T,np.dot(Phi,vecPatch))

    # subtract mean
    if Flags.subMean == 1:
        Measurements = Measurements - np.mean(Measurements)

    if Flags.patch_loc:
        M_orig = 2*num*Config.patchSize[0]*Config.patchSize[1]
        M = M_orig*24
        pos = _get_pos(loc, M_orig)
        temp = np.zeros(M)
        temp[pos:pos+M_orig] = Measurements
        Measurements = temp

    return Measurements, Phi, loc


def _get_one_hot(loc):
    n_y = 4     # tile height
    n_x = 6     # tile width
    y = loc[0]  # position on tile
    x = loc[1]  # position on tile

    vec = np.zeros(n_y*n_x)
    vec[y*n_x+x] = 1

    return vec


def _get_pos(loc, M_orig):
    n_y = 4     # tile height
    n_x = 6     # tile width
    y = loc[0]  # position on tile
    x = loc[1]  # position on tile

    return M_orig*(y*n_x+x)


def create_training_data(data_in, formPhi_func, phitype):
    # unpack data_in
    TT = data_in[0]
    numTrainingLightFields = data_in[1]

    # number of training patches
    numTrainingPatches = Config.numTrainingPatches
    lightFieldRes = Config.lightFieldRes

    # size of a patch [spatial_y spatial_x angular_y angular_x]
    if Flags.neighbor == 0:
        patchSize = Config.patchSize
    else:
        patchSize = Config.patchSize_neighbor

    num = Config.diagPhi_num if Flags.diagPhi == 1 else 1

    ############################################################
    # now get random patches and simulate measurements on sensor
    if Flags.create_data == 1:
        Patches = np.zeros((numTrainingPatches,)+(1,)+patchSize)
        # number of Measurements for each patch
        if Flags.interpolate is False:
            M = 2*num*patchSize[0]*patchSize[1]
        else:
            M = np.prod(patchSize)

        M = M*24 if Flags.patch_loc else M
        # Measurements : [numTrainingPatches, nColor, numMeasurements per patch]
        Measurements = np.zeros((numTrainingPatches,1,M))

        # Seed to reproduce(change this out if different results required)
        sd1 = 20
        np.random.seed(sd1)
        status = 0
        k = 0
        while k < numTrainingPatches:
            if k >= status:
                print "Iteration: ", k
                status += 5000
            N = np.random.randint(0,numTrainingLightFields) #pick which LF
            y = np.random.randint(0,lightFieldRes[0]-patchSize[0]) #pick y
            x = np.random.randint(0,lightFieldRes[1]-patchSize[1]) #pick x
            Patches[k,0,:,:,:,:] = TT[N,0,y:y+patchSize[0],x:x+patchSize[1],:,:]
            vecPatch = np.ravel(Patches[k,0,:,:,:,:])

            Measurements[k, 0, :], Phi, loc = _patch_to_measurement(vecPatch, formPhi_func, patchSize, y, x, num)

            #Measurements[k,0,:] = np.concatenate((_measurement,_get_one_hot(loc))) if Flags.patch_loc else _measurement

            k += 1

        print "Measurements shape: " , Measurements.shape
        print 'Phi shape: ', Phi.shape

        # split into train and validation (must be called data, label)
        if Flags.neighbor == 0:
            patchSizeh5py = patchSize
            sx_start = 0
            sx_end = 11
            sy_start = 0
            sy_end = 11
        else:
            patchSizeh5py = (11,11,5,5)
            sx_start = 2
            sx_end = 13
            sy_start = 2
            sy_end = 13

        splitPer = 0.85
        split = int(np.floor(numTrainingPatches*splitPer))

        label = np.zeros((split,1,np.prod(patchSizeh5py)),dtype='float64')
        label_val = np.zeros((numTrainingPatches-split,1,np.prod(patchSizeh5py)),dtype='float64')

        data = np.zeros((split,1,M),dtype='float64')
        data_val = np.zeros((numTrainingPatches - split,1,M),dtype='float64')

        # size tests
        print 'trainlabelshape = ' +str(label.shape)
        print 'vallabelshape = ' + str(label_val.shape)

        for k in range(0,numTrainingPatches):
            if k >= split:
                data_val[k-split,0,:] = Measurements[k,0,:]
                if Flags.weighted == True:
                    label_val[k-split,0,:] = wl.weighted_loss(np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:]),patchSize)
                else:
                    label_val[k-split,0,:] = np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:])
            else:
                data[k,0,:] = Measurements[k,0,:]
                if Flags.weighted == True:
                    label[k,0,:] = wl.weighted_loss(np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:]),patchSize)
                else:
                    label[k,0,:] = np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:])

        #create hdf5 formats compatible with Caffe
        tfn = "train-ASPphi%s-N=%d-%d-%d.h5"%(phitype,num,sd1,numTrainingPatches)
        vfn = "val-ASPphi%s-N=%d-%d-%d.h5"%(phitype,num,sd1,numTrainingPatches)
        f  = h5py.File(tfn,"w")
        f['data'] = data.astype('float64')
        f['label'] = label.astype('float64')
        f.close()

        f2 = h5py.File(vfn,"w")
        f2['data'] = data_val.astype('float64')
        f2['label'] = label_val.astype('float64')
        f2.close()
        print "Dumped: %s, %s at %s" % (tfn,vfn,os.getcwd())

        # Dump weightl2 in h5 format
        if Flags.weighted == True:
            wl.dump_weight_h5(patchSize)
    else:
        print 'Enable data_create flag to generate train/val h5 files'


if __name__ == "__main__":
    imgpath = os.path.join(sys.argv[1])
    lytroimg_to_lf(imgpath)
    #load_lf_to_tensor(sys.argv[1], [sys.argv[2]])
    #load_lf_to_tensor()
