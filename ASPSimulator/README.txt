# Fast Reconstruction of 4D Light Fields from Angle Sensitive Pixel Capture using Deep Neural Networks

## Abstract
Capturing 4D light fields, optical representations of scenes used in computer 
graphics and vision, typically requires bulky optical setups and camera arrays or
sacrificing spatial resolution in single-shot camera methods. In particular, Angle
Sensitive Pixels (ASPs) are diffractive CMOS image sensors that can encode the
4D light field onto the 2D sensor, but necessitate the need for dictionary learning
and sparse coding algorithms to recover the 4D light field at high spatial resolution.
These iterative solvers can take up to hours to reconstruct a single light field,
prohibiting the capture of light field video. In this paper, we present a new deep
neural network architecture to invert ASP light field measurements and recover the
high resolution 4D light field significantly faster than previous methods. We show
experimental results on simulated light fields to demonstrate the feasibility of our
approach and the possibility of doing real-time light field reconstructions.

## Source File Description
train_validate.prototxt                 ==> Network definition file
runLFnet.sh                             ==> script to launch training on network
create_hdf5_4Dreconnet.py               ==> Python script to create train/val data for 4D reconnet(not used)
LFNetData                               ==> Contains caffemodel trained on Compressed Sense lightfield with 800:3025 measurement ratio
animateLightField.py                    ==> Python script to animate lightfield
LFReconstructed.mp4
gatherPatches.py                        ==> Main script to divide lightfield into patches and split them in train/val, dumps h5 files
train-ASPphi-10.h5                      ==> sample h5 file
val-ASPphi-10.h5                        ==> sample h5 file
deploy_8x8x5x5.prototxt                 ==> deploy file suitable for 8x8x5x5 patchsize
deploy.prototxt                         ==> original deploy file
old                                     ==> folder containing old unused files
LFReconstructed_15000-18.03.mp4
deploy_11x11x5x5_242.prototxt           ==> deploy file suitable for 11x11x5x5 patchsize
calcPSNR.py                             ==> File to calculate PSNR for reconstructed lightfield
LFOriginal.mp4
LFReconstructed_15000-30.60.mp4
test_LFReconNet.py                      ==> Main File to reconstruct lightfield using a trained caffemodel file
deploy_8x8x3x3.prototxt                 ==> deploy file suitable for 8x8x3x3 patchsize
train.txt                               ==> points to training data
test.txt                                ==> points to validation data
solver.prototxt                         ==> Solver file for caffe
ASPSimulator                            ==> Folder containing similar files but tuned for ASP lightfields and Real data.
old_Matlab_Files                        ==> Old unused matlab files
results                                 ==> Contains mp4 results files

CompressiveLightFieldPhotography-DatasetsAndCode ==> Synthetic lightfield folder used to create training data in gatherPatches.py file

