""" Authors Initials: SJ/AJ
    This script generates compressed training/validation data by pre-processing lightfield dumps
"""
from lfreconpack import get_training_data as gtd
from lfreconpack import formPhi as fm

if __name__ == "__main__":
    # x = y*phiSim, y = Synthetic LF
    gtd.create_training_data(gtd.load_lf_to_tensor(), fm.formPhiSim, 'Sim')
