# Flags
class Flags:
    create_data = 1     # Enable data creation
    verbose = 1         # Verbose mode
    vverbose = 0        # Extra verbose mode
    animate = 0         # Enable animatation of input and output
    saveanim = True     # Enable to save the animation
    overlap = False     # Reconstruction using overlapping patches
    normalizePhi = 0    # To normalizePhi
    neighbor = 0        # Enable neighboring pixel info
    subMean =0          # Subtract mean from Measurements
    diagPhi = 0         # Adding diag flag
    weighted = False    # To enable weighted l2 loss function
    interpolate = False  # To enable linear interpolation i.e. multiplication by Phi.T
    patch_loc = False   # To enable one hot location vector as part of training input

# Configuration
class Config:
    patchSize = (11,11,5,5)
    patchSize_neighbor = (15,15,5,5)
    lightFieldRes = (593,840,5,5)
    numTrainingPatches = 50000
    lightFieldAngularResponseRes = (356,372,5,5)
    diagPhi_num = 6

    # Test time config
    # set paths to model, weights
    Model_file = '../deploy_6fc.prototxt'
    Weights = './sweepResults/BigNet/ASP_Diag_N=1-6fc_iter_100000.caffemodel'
    # Set stride
    # This setting is for non-overlapping
    stridey = patchSize[0]
    stridex = patchSize[1]

def print_flags():
    print create_data
    print verbose
    print vverbose
    print animate
    print saveanim
    print overlap
    print normalizePhi
    print neighbor
    print subMean
    print diagPhi
    print weighted
    print interpolate
    print patch_loc

def print_config():
    print patchSize
    print patchSize_neighbor
    print lightFieldRes
    print numTrainingPatches
    print lightFieldAngularResponseRes
    print diagPhi_num

    # Test time config
    # set paths to model, weights
    print Model_file
    print Weights
