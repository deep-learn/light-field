#!/usr/bin/env sh

#./build/tools/caffe train --solver=examples/light-field/solver.prototxt -gpu 0
#./build/tools/caffe train --solver=examples/light-field/solver.prototxt -weights ./ASP_Diag_N=1-6fc-denoise_iter_10000.caffemodel -gpu all
#./build/tools/caffe train --solver=examples/light-field/solver.prototxt -gpu 0 2>&1 | tee ASP_Sim_N1_6fc_1patch_lr1e6.log && ./tools/extra/parse_log.sh ASP_Sim_N1_6fc_1patch_lr1e6.log && ./tools/extra/plot_training_log.py.example 6 ./TrainingLoss.png ./ASP_Sim_N1_6fc_1patch_lr1e6.log
./build/tools/caffe train --solver=examples/light-field/solver.prototxt -weights ./ASP_Sim_N1-6fc-10patch-lr1e-6_iter_11000.caffemodel -gpu 1 2>&1 | tee ASP_Sim_N1_6fc_10patch2_lr1e6.log
