## Arjun Jauhari
##
## Show a wiggling light field.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib.animation as animation

def animateLightField(lightField, filename=None, saveanim=False):

    # expected [angle_y angle_x spatial_y spatial_x numColorChannels]
    lightFieldResolution = lightField.shape;

    # indices for animation
    centerY     = np.round(lightFieldResolution[0]/2);
    indicesY    = np.arange(centerY,-1,-1);
    indicesY    = np.concatenate((indicesY, np.arange(1,lightFieldResolution[0])));
    indicesY    = np.concatenate((indicesY, np.arange(lightFieldResolution[0]-2,centerY-1,-1)));

    centerX     = np.round(lightFieldResolution[1]/2);
    indicesX    = np.arange(centerX,-1,-1);
    indicesX    = np.concatenate((indicesX, np.arange(1,lightFieldResolution[1])));
    indicesX    = np.concatenate((indicesX, np.arange(lightFieldResolution[1]-2,centerX-1,-1)));

    numColorChannels = 1;
    if np.size(lightFieldResolution) > 4:
        numColorChannels = lightFieldResolution[4];

    frameCount = 1;
    pauseTime = 0.5;

    ## create figure object
    fig = plt.figure()

    ## create animation
    anim = animation.FuncAnimation(fig, animate, fargs=(indicesX,indicesY,lightFieldResolution,lightField,centerX,centerY,numColorChannels), frames=np.size(indicesX)+np.size(indicesY), interval=pauseTime*1000, blit=True, repeat=True);

    plt.axis('off')
    
    # Save the animation
    if saveanim == True:
        anim.save(filename, fps=3, writer='mencoder')
    plt.show()

## Update function
def animate(i,indicesX,indicesY,lightFieldResolution,lightField,centerX,centerY,numColorChannels):
    if (i < np.size(indicesX)):
        # use indicesX
        idx = indicesX[i];
        I = np.reshape(lightField[centerY,idx,:,:,:], (lightFieldResolution[2], lightFieldResolution[3], numColorChannels));
    else:
        # use indicesY
        idx = indicesY[i-np.size(indicesX)];
        I = np.reshape(lightField[idx,centerX,:,:,:], (lightFieldResolution[2], lightFieldResolution[3], numColorChannels));
    if numColorChannels == 1:
        return [plt.imshow(I.squeeze(), cmap="Greys_r")]; # make it iterable and return
    else:
        return [plt.imshow(I.squeeze())]; # make it iterable and return


## Test
## load original light field
## number of pixels in [angle_y angle_x spatial_y spatial_x numColorChannels]
#lightFieldResolution = (5, 5, 593, 840, 3);
#lightField = np.zeros(lightFieldResolution);
#
#for ky in range(lightFieldResolution[0]):
#    for kx in range(lightFieldResolution[1]):
#        #I = mpimg.imread('/home/molnargroup/Documents/CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldResolution[1]+kx+1) + '.png');
#        I = mpimg.imread('/home/arjun/Desktop/Cornell_courses/deeplearning/CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldResolution[1]+kx+1) + '.png');
#        lightField[ky,kx,:,:,:] = np.reshape(I, (1, 1, lightFieldResolution[2], lightFieldResolution[3], 3));
#animateLightField(lightField,1)
