
clear all; 
close all;
clc;

addpath('..\3rdparty\spgl1-1.7');
addpath('..\3rdparty\');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% recover only a small test region
bTestRegion = true;

% 0 is patch-by-patch (faster), 1 is sliding patches (much slower)
patchMode   = 0;

% 0 is for dual phase reconstruction, 1 is for differential reconstruction
bRecoverDiff = 0;

 % using this at 255 gives way better parallax
 scalingFactor = 255;


% matrix normalization
% 0 - normalize nothing
% 1 - normalize rows
% 2 - normalize columns
matrixNormalization = 2;


% aperture size (mm) for filenames
apertureSize = 1;


% calibration data


datapath_calibration    = '..\DATA\Calibration Datasets\White_PigImpulse\';
filename_calibration    = 'ImpulseCal_';

% datapath_calibration    = '..\DATA\Calibration Datasets\Red_PigImpulse\';
% filename_calibration    = 'ImpulseCal_';
% 
% datapath_calibration    = '..\DATA\Calibration Datasets\Blue_PigImpulse\';
% filename_calibration    = 'ImpulseCal_';
% 
% datapath_calibration    = '..\DATA\Calibration Datasets\Green_PigImpulse\';
% filename_calibration    = 'ImpulseCal_';

% path to data

datapath                = '..\DATA\Datasets\PigDataset\';

%filename                = 'White_ktep_firstscene_p6.mat';
 filename                = 'White_ktep_secondscene_p6.mat';
% filename                = 'White_ktep_thirdscene_p6.mat';
% filename                = 'White_ktep_fourthscene_p6.mat';
% filename                = 'White_ktep_fifthscene_p6.mat';
% filename                = 'Red_ktep_firstcene_p6.mat';
% filename                = 'Red_ktep_secondscene_p6.mat';
% filename                = 'Red_ktep_thirdscene_p6.mat';
% filename                = 'Red_ktep_fourthscene_p6.mat';
% filename                = 'Red_ktep_fifthscene_p6.mat';
% filename                = 'Green_ktep_firstcene_p6.mat';
% filename                = 'Green_ktep_secondscene_p6.mat';
% filename                = 'Green_ktep_thirdscene_p6.mat';
% filename                = 'Green_ktep_fourthscene_p6.mat';
% filename                = 'Green_ktep_fifthscene_p6.mat';
% filename                = 'Blue_ktep_firstcene_p6.mat';
% filename                = 'Blue_ktep_secondscene_p6.mat';
% filename                = 'Blue_ktep_thirdscene_p6.mat';
% filename                = 'Blue_ktep_fourthscene_p6.mat';
% filename                = 'Blue_ktep_fifthscene_p6.mat';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% load dictionary
%load('..\..\SyntheticExperiments\DictionaryLearning\LightField4D-Dictionary-Text-500kPatches-8x8.mat'); % this is for text!
load('..\DictionaryLearning\LightField4D-Dictionary_9x9.mat'); % this is a better dictionary

% max iterations for optimization
maxIters = 100*patchSize(3)*patchSize(4);

% draw intermediate results
bDraw = 0;

% animate intermediate results
bAnim = false;

% print progress and current error
echoProgress = true;

% solver:
%   0 - SPGL1
%   1 - ADMM
bReconstructionMethod = 1;

% set global lambda for ADMM
%admmlambda = 0.01;
%new value tuned by Gordon
admmlambda = 0.00001;
%admmlambda = 0.000001;

numASP = 24;
ASPTile = [4,6];
ASPTiles = [94,64]; % ASP is 96x64, dict is 5x5


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize light field

% number of pixels in [angle_y angle_x spatial_y spatial_x numColorChannels]

lightFieldResolution = [5 5 356 372];
lightField = zeros(lightFieldResolution);
       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % load measurements

    load([datapath filename]);
    sensorImage     = im1;
    sensorImageDual = im2;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load calibration images

    lightFieldAngularResponse       = zeros(lightFieldResolution);
    lightFieldAngularResponseDual   = zeros(lightFieldResolution);

    for ky=1:lightFieldResolution(1)
        for kx=1:lightFieldResolution(2)

            % load data
            kky = lightFieldResolution(1)-ky+1;          
            kkx = kx;

            filename_resp = [datapath_calibration filename_calibration sprintf('%d%d', kky, kkx)];
            load(filename_resp);

            lightFieldAngularResponse(ky,kx,:,:)        = reshape(im1, [1 1 lightFieldResolution(3) lightFieldResolution(4)]);
            lightFieldAngularResponseDual(ky,kx,:,:)    = reshape(im2, [1 1 lightFieldResolution(3) lightFieldResolution(4)]);
            
        end
    end
   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% crop small test region if desired

if bTestRegion
    % these coordinates are in multiples of the tile resolution        
   
        %cropCoords  = [5 3];
        %cropSize    = [10 12];    
        
        %cropCoords  = [20 18];
        %cropSize    = [25 15];    
        
        cropCoords  = [13 20];
        cropSize    = [30 30];    
        %cropCoords  = [40 20];
        %cropSize    = [3 12];    
          
    
    % crop angular responses
   
        lightFieldAngularResponse = lightFieldAngularResponse(:,:,  (cropCoords(1)-1)*ASPTile(1)+1:(cropCoords(1)-1)*ASPTile(1) + ASPTile(1)*cropSize(1),...
                                                                    (cropCoords(2)-1)*ASPTile(2)+1:(cropCoords(2)-1)*ASPTile(2) + ASPTile(2)*cropSize(2)    );

        lightFieldAngularResponseDual = lightFieldAngularResponseDual(:,:,  (cropCoords(1)-1)*ASPTile(1)+1:(cropCoords(1)-1)*ASPTile(1) + ASPTile(1)*cropSize(1),...
                                                                            (cropCoords(2)-1)*ASPTile(2)+1:(cropCoords(2)-1)*ASPTile(2) + ASPTile(2)*cropSize(2)    );                                                        
        % crop sensor image           
        sensorImage = sensorImage(          (cropCoords(1)-1)*ASPTile(1)+1:(cropCoords(1)-1)*ASPTile(1) + ASPTile(1)*cropSize(1),...
                                            (cropCoords(2)-1)*ASPTile(2)+1:(cropCoords(2)-1)*ASPTile(2) + ASPTile(2)*cropSize(2)    );
        sensorImageDual = sensorImageDual(  (cropCoords(1)-1)*ASPTile(1)+1:(cropCoords(1)-1)*ASPTile(1) + ASPTile(1)*cropSize(1),...
                                            (cropCoords(2)-1)*ASPTile(2)+1:(cropCoords(2)-1)*ASPTile(2) + ASPTile(2)*cropSize(2)    );
        lightFieldResolution(3) = size(sensorImage,1);
        lightFieldResolution(4) = size(sensorImage,2);
end      

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% patch-by-patch reconstruction (fast)

if patchMode == 0
        
    numImagePatches = [floor(lightFieldResolution(3)/patchSize(3)) floor(lightFieldResolution(4)/patchSize(4))];
    
    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);

    % reconstruct all the patches
    totalTime = tic;

    
   for ky=1:numImagePatches(1)
      for kx=1:numImagePatches(2)
                                                
                xStart  = (kx-1)*patchSize(4)+1;
                xEnd    = kx*patchSize(4);

                yStart  = (ky-1)*patchSize(3)+1;
                yEnd    = ky*patchSize(3);
                                                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % construct projection matrix                                
                
                              
                    lfPatchResponses        = lightFieldAngularResponse(:,:,yStart:yEnd,xStart:xEnd);
                    lfPatchResponsesDual    = lightFieldAngularResponseDual(:,:,yStart:yEnd,xStart:xEnd);
                   
                    R = [lfPatchResponses(:); lfPatchResponsesDual(:)];
                
                                
                                   
                    % construct measurement matrix 
                    if bRecoverDiff ==1
                    PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                    count = 1;
                    offset = prod(patchSize);
                    for kk=1:patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) - R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end

                    count = 1;
                    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            (R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) + R(count:count+patchSize(1)*patchSize(2)-1)) / 8;
                        count = count + patchSize(1)*patchSize(2);
                    end
                    else
               
                 PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                    count = 1;
                    for kk=1:patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end
                    count = 1;
                    offset = prod(patchSize);
                    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end
                    end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % extract patch from measured data
                
                    I1 = sensorImage(yStart:yEnd,xStart:xEnd);
                    I2 = sensorImageDual(yStart:yEnd,xStart:xEnd);
                    
                    % measurements
                    if bRecoverDiff ==1
                          I = [I2(:)-I1(:); (I1(:)+I2(:))/8];
                    else
                      I = [I1(:); I2(:)];
                    end
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % reconstruct patch with SPGL1                
                
                if bReconstructionMethod == 0
                    
                    sigma = 0.001;    % Desired ||Ax - b||_2
                    
                    % options
                    opts = spgSetParms( 'verbosity', 0,...
                        'iterations', maxIters );
                    
                    % reconstruct sparse coefficients
                    currentTime = tic;
                    
                    % normalize matrix rows
                    if matrixNormalization == 1
                        MM=diag(1./sqrt(sum((PHI*D).^2,2)));                        
                        
                        % verify - works
%                        plot(sqrt(sum( (MM*PHI*D).^2, 2)));
                        
                        alpha   = spg_bpdn(MM*PHI*D, MM*I, sigma, opts);
                    % normalize matrix columns
                    elseif matrixNormalization == 2
                        NN=diag(1./sqrt(sum((PHI*D).^2,1)));
                        
                        % verify - works
                        %plot(sqrt(sum( (PHI*D*NN).^2, 1 )))
                        
                        
                        alpha = spg_bpdn(PHI*D*NN, I, sigma, opts);
                        alpha = NN*alpha;

                    else
                        alpha   = spg_bpdn(PHI*D, I, sigma, opts);
                    end
                    timeElapsed = toc(currentTime);
                    
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % reconstruct patch with ADMM
                    
                else
                    
                    currentTime = tic;
                    
                    
                     % normalize matrix rows
                    if matrixNormalization == 1
                        
                        MM=diag(1./sqrt(sum((PHI*D).^2,2)));                        
                        alpha = lassoADMM(MM*PHI*D, I, admmlambda, 1.0, 1.0);
                    
                    % normalize matrix columns
                    elseif matrixNormalization == 2
                        
                        NN=diag(1./sqrt(sum((PHI*D).^2,1)));
                        alpha = lassoADMM(PHI*D*NN, I, admmlambda, 1.0, 1.0);
                        alpha = NN*alpha;

                    else                    
                        alpha = lassoADMM(PHI*D, I, admmlambda, 1.0, 1.0);
                    end
                    
                    timeElapsed = toc(currentTime);
                    
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                
                % reconstruct patch
                Prec    = D*alpha;
                
                % clamp
                Prec(Prec<0)=0;
                Prec(Prec>scalingFactor)=scalingFactor;
                
                % plot progress and error
                if echoProgress
                   disp(['patch ' num2str(  (ky-1)*numImagePatches(2) + kx) ' of ' num2str(numImagePatches(1)*numImagePatches(2)) ', took (in ms): ' num2str(timeElapsed)]);
                end
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % sort into reconstruction
                lightFieldRec(:,:, (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4)) = reshape(Prec, patchSize);
                         
             
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       
                
      end
   end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
    % save data
    
    if bAnim
        animateLightField(lightFieldRec./max(lightFieldRec(:)), 2);
    end
    
    %normalize recovered light field
    lightFieldRec = (lightFieldRec - min(lightFieldRec(:)))/(max(lightFieldRec(:))-min(lightFieldRec(:)));
        
    if bTestRegion
        savefilename = ['LightFieldReconstruction-PatchByPatch-TestRegion-' filename '.mat'];
    else        
        savefilename = ['LightFieldReconstruction-PatchByPatch-' filename '.mat'];
    end    
    save(savefilename,  'lightFieldRec','elapsedTimeTotal', ...
                        'datapath_calibration', 'filename_calibration', 'datapath', 'filename' );
        
else
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % sliding patch reconstruction (very slow)
    
    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);
    
    numImagePatches = [lightFieldResolution(3)-patchSize(3) lightFieldResolution(4)-patchSize(4)];
    
       
    % reconstruct all the patches
    totalTime = tic;
    for ii=1:numImagePatches(1)*numImagePatches(2)        
       
        % current color channel
        c  = 1;
        % current patch index in y
        ky = floor( ((ii - (c-1)*numImagePatches(1)*numImagePatches(2))-1) / numImagePatches(2) ) + 1;
        % current patc hindex in x
        kx = mod(ii-1,numImagePatches(2))+1;
                        
       

        xStart  = kx;
        xEnd    = kx+patchSize(4)-1;

        yStart  = ky;
        yEnd    = ky+patchSize(3)-1;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % construct projection matrix                                

                    
            lfPatchResponses        = lightFieldAngularResponse(:,:,yStart:yEnd,xStart:xEnd);
            lfPatchResponsesDual    = lightFieldAngularResponseDual(:,:,yStart:yEnd,xStart:xEnd);

            R = [lfPatchResponses(:); lfPatchResponsesDual(:)];
       
       

            % construct measurement matrix 
            if bRecoverDiff==1
            PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
            count = 1;
            offset = prod(patchSize);
            for kk=1:patchSize(3)*patchSize(4)
                PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                    R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) - R(count:count+patchSize(1)*patchSize(2)-1);
                count = count + patchSize(1)*patchSize(2);
            end

            count = 1;
            for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                    (R(count+offset:count+offset+patchSize(1)*patchSize(2)-1) + R(count:count+patchSize(1)*patchSize(2)-1)) / 8;
                count = count + patchSize(1)*patchSize(2);
            end
            else
               PHI = zeros([2*patchSize(3)*patchSize(4) prod(patchSize)]);
                    count = 1;
                    for kk=1:patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end
                    count = 1;
                    offset = prod(patchSize);
                    for kk=patchSize(3)*patchSize(4)+1:2*patchSize(3)*patchSize(4)
                        PHI(kk, count:count+patchSize(1)*patchSize(2)-1 ) =  ...
                            R(count+offset:count+offset+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end 
            end
       
                
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % extract patch from measured data

        
            I1 = sensorImage(yStart:yEnd,xStart:xEnd);
            I2 = sensorImageDual(yStart:yEnd,xStart:xEnd);

            % measurements
            
               if bRecoverDiff==1
                I = [I2(:)-I1(:); (I1(:)+I2(:))/8];
               else
                   I = [I1(:); I2(:)];
               end
            

       

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % reconstruct patch with SPGL1                

        if bReconstructionMethod == 0

            sigma = 0.001;    % Desired ||Ax - b||_2

            % options
            opts = spgSetParms( 'verbosity', 0,...
                'iterations', maxIters );

            % reconstruct sparse coefficients
            currentTime = tic;

            % normalize matrix rows
            if matrixNormalization == 1
                MM=diag(1./sqrt(sum((PHI*D).^2,2)));                        

                % verify - works
%                        plot(sqrt(sum( (MM*PHI*D).^2, 2)));

                alpha   = spg_bpdn(MM*PHI*D, MM*I, sigma, opts);
            % normalize matrix columns
            elseif matrixNormalization == 2
                NN=diag(1./sqrt(sum((PHI*D).^2,1)));

                % verify - works
                %plot(sqrt(sum( (PHI*D*NN).^2, 1 )))


                alpha = spg_bpdn(PHI*D*NN, I, sigma, opts);
                alpha = NN*alpha;

            else
                alpha   = spg_bpdn(PHI*D, I, sigma, opts);
            end
            timeElapsed = toc(currentTime);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % reconstruct patch with ADMM

        else
                    
            currentTime = tic;


             % normalize matrix rows
            if matrixNormalization == 1

                MM=diag(1./sqrt(sum((PHI*D).^2,2)));                        
                alpha = lassoADMM(MM*PHI*D, I, admmlambda, 1.0, 1.0);

            % normalize matrix columns
            elseif matrixNormalization == 2

                NN=diag(1./sqrt(sum((PHI*D).^2,1)));
                alpha = lassoADMM(PHI*D*NN, I, admmlambda, 1.0, 1.0);
                alpha = NN*alpha;

            else                    
                alpha = lassoADMM(PHI*D, I, admmlambda, 1.0, 1.0);
            end

            timeElapsed = toc(currentTime);

        end
                
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % reconstruct patch
        Prec    = D*alpha;

        % clamp
        Prec(Prec<0)=0;
        Prec(Prec>scalingFactor)=scalingFactor;

        % plot progress and error
        if echoProgress
         
                disp(['patch ' num2str(  (ky-1)*numImagePatches(2) + kx) ' of ' num2str(numImagePatches(1)*numImagePatches(2)) ', took (in ms): ' num2str(timeElapsed)]);
        end
       

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % sort into reconstruction
        lightFieldRec(:,:, yStart:yEnd,xStart:xEnd) = ...
            lightFieldRec(:,:, yStart:yEnd,xStart:xEnd) + reshape(Prec, patchSize);



              
    end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
   %normalize recovered light field
    lightFieldRec = (lightFieldRec - min(lightFieldRec(:)))/(max(lightFieldRec(:))-min(lightFieldRec(:)));

    
    if bTestRegion
        savefilename = ['LightFieldReconstruction-SlidingPatch-TestRegion-' filename '.mat'];
    else        
        savefilename = ['LightFieldReconstruction-SlidingPatch-' filename '.mat']; %added test, Suren
    end    
    save(savefilename,  'lightFieldRec','elapsedTimeTotal', ...
                        'datapath_calibration', 'filename_calibration', 'datapath', 'filename' );

    
end

figure, drawLightField4D(lightFieldRec);

%% Writing out light field and filtering it
fn = ['..\reconstruct\',savefilename];
writeOutLightField(fn);
fn = ['..\reconstruct\',savefilename(1:end-4),'\'];
reconfilt(fn);
