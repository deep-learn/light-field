function writeOutLightField(fn)

load(fn)

mn = min(min(squeeze(lightFieldRec(ceil(end/2),ceil(end/2),:,:))));
lightFieldRec = lightFieldRec + mn;
mx = max(max(squeeze(lightFieldRec(ceil(end/2),ceil(end/2),:,:))));
lightFieldRec = uint8(255.0 .* lightFieldRec ./ mx);

[path, name, ext] = fileparts(fn);
mkdir(path,name);

for yy = 1:size(lightFieldRec,1)
    for xx = 1:size(lightFieldRec,2)
        outname = sprintf('%s/%s/%d%d.png',path,name,size(lightFieldRec,1)-yy+1,xx);
        imwrite(squeeze(lightFieldRec(yy,xx,:,:)),outname);
    end
end
