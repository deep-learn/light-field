function g = makeGauss(sz,scale,width)

p1 = [179,249];
p2 = [179,125];
p3 = [179, 63];
p4 = [179,311];

p5 = [90, 249];
p6 = [268,125];
p7 = [90, 125];
p8 = [268,249];

p9 = [90, 311];
p10= [268, 63];
p11= [90,  63];
p12= [268,311];

p13= [90, 187];
p14= [268,187];

p15= [90, 156];
p16= [90, 218];
p17= [268, 156];
p18= [268, 218];

p19= [179, 372];
p20= [179, 0];

p21= [90, 372];
p22= [268,372];
p23= [90, 0];
p24= [268,0];

p25= [228,250];
p26= [228,122];
p27= [127,122];
p28= [127,250];

g = zeros(sz);

g = g + gauss2d(g,width*3,p1);
g = g + gauss2d(g,width*3,p2);
g = g + gauss2d(g,width*2,p3);
g = g + gauss2d(g,width*2,p4);
g = g + gauss2d(g,width,p5);
g = g + gauss2d(g,width,p6);
g = g + gauss2d(g,width,p7);
g = g + gauss2d(g,width,p8);
g = g + gauss2d(g,width,p9);
g = g + gauss2d(g,width,p10);
g = g + gauss2d(g,width,p11);
g = g + gauss2d(g,width,p12);
g = g + gauss2d(g,width,p13);
g = g + gauss2d(g,width,p14);
g = g + gauss2d(g,width,p15);
g = g + gauss2d(g,width,p16);
g = g + gauss2d(g,width,p17);
g = g + gauss2d(g,width,p18);
g = g + gauss2d(g,width,p19);
g = g + gauss2d(g,width,p20);
g = g + gauss2d(g,width,p21);
g = g + gauss2d(g,width,p22);
g = g + gauss2d(g,width,p23);
g = g + gauss2d(g,width,p24);
g = g + gauss2d(g,width,p25);
g = g + gauss2d(g,width,p26);
g = g + gauss2d(g,width,p27);
g = g + gauss2d(g,width,p28);

g = 1 - (g * scale);