function reconfilt(path)
% filter image

a = imread(sprintf('%s/%d%d.png',path,1,1));
g = makeGauss(size(a),0.8,120);
for x=1:5
    for y=1:5
        a = real(ifft2(ifftshift(g).*fft2(imread(sprintf('%s/%d%d.png',path,x,y)))));
        imwrite(uint8(a),sprintf('%s/%d%d_f.png',path,x,y));
    end
end