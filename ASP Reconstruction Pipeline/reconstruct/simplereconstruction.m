%reconstruct light field, simple linear algorithm




%load parameters
loadASPparam;

%Tile pattern
nfSort1=[47 35  5 43 13 19 ...
          7 45  1 29 39 27 ...
         31 17  9 21 15 33 ...
         37 41 25  3 23 11];
nfSort2= nfSort1 + 1;

% construct image
for i=5:64-5
    for j=5:96-5
        for zz=1:24
            lamb = nfSort1(zz);
    lamb2 = nfSort2(zz);
    i1 = frame{lamb};
    i2 = frame{lamb2};
    
    image(i,j,zz) = i1(i,j) - i2(i,j);
    
    
            
            
        end
        image(i,j,25) = i1(i,j) + i2(i,j);
        
        
    end
end



% Construct ASP transform matrix, stored in ASP variable. GW_sj = makes
% gabor wavelet corresponding to each type of ASP 

M = 5;   % n samples in response (long axis)
A = 0.5; % peak amplitude of response
S = 2;%1.4; % scale of gaussian window
g = w(49).*Gw_sj(M,A,par(49,:),S);
for zz=1:24 
    lamb = nfSort1(zz);
    lamb2 = nfSort2(zz);
temp =1/2*w(lamb).*Gw_sj(M,A,par(lamb,:),S);  %added 1/2 factor (empirically)   
temp2 = 1/2*w(lamb2).*Gw_sj(M,A,par(lamb2,:),S);
ASP(zz,:) = temp(:)' - temp2(:)';
end
temp = 1/2*w(49).*Gw_sj(M,A,par(49,:),S);
ASP(25,:) = temp(:)';

%pASP = pinv(ASP);
pASP = ASP';
for y=1:size(image,1)
    for x=1:size(image,2)
        
        lf = pASP*squeeze(image(y,x,:));
        lightFieldRec(:,:,y,x) = reshape(lf,[5 5]);
        
    end
end
 lightFieldRec = (lightFieldRec - min(lightFieldRec(:)))./(max(lightFieldRec(:))-min(lightFieldRec(:)));
 figure, drawLightField4D(lightFieldRec);
 figure, animateLightField(lightFieldRec,1);
