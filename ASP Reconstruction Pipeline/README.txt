README:

ASP Reconstruction pipeline for real/synthetic data using both nonlinear and linear reconstructions.

Folders:
3rd party - list of external algorithms for l1 minimization used in reconstruction
DATA - contains data from prototype as well as associated calibration data for impulse response
Light_Fields - synthetic light fields
DictionaryLearning - contains dictionaries used in reconstruction
reconstruct - folder to reconstruct real data
reconstruct_linear - linear reconstruction for real and synthetic data
reconstruct_synthetic - nonlinear reconstruction for synthetic data

the main file in each of the three reconstruction files is of the form: reconstructLightField.m (real data, nonlinear recon), reconstructLightFieldTwoPhase_synthetic.m (synthetic data, nonlinear recon), or reconstructLightField_linear.m (linear folder)

There are many parameters in each of these files, but I'll outline some common ones (not all reconstructions will have all these parameters):

bTestRegion - reconstruct a small portion of the image

patchmode - patch by patch for speed, sliding patch for quality

bRecoverDiff - whether to operate in differential mode (for ASP data) or two phase mode 


Output of reconstruction: lightFieldRec, usually a 4D function (sometimes 5D if color was obtained for synthetic experiments)


Common functions to view the lightfield:
drawLightField4D(lightFieldRec)(draws the array of perspective views)
animateLightField(lightFieldRec,N) - where N is the number of loops the movie should play (I usually set it to 2)


Saving:
Make sure if you run experiments, you save lightFieldRec to a .mat file with an appropriate name for the experiment. Be careful to not overwrite .mat files, especially when running multiple experiments on NightBird. 

