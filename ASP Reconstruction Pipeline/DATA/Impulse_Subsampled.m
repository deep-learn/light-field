clear Z0 Z W5
for nASP=1:48 %crops impulse
     Z0(:,:,nASP)=flipud(frame{nASP}(:,1:96)');
     Z(:,:,nASP)=Z0(18:70,26:52,nASP);
end

x=0:4:4*size(Z,1)-1;
y=0:6:6*size(Z,2)-1;

x1=max(x)/10:max(x)/5:max(x);
y1=max(y)/10:max(y)/5:max(y);

[X,Y]=meshgrid(y,x);[X1,Y1]=meshgrid(y1,x1);

for nASP=1:48
     W5(:,:,nASP)=interp2(X,Y,squeeze(Z(:,:,nASP)),X1,Y1);
end
figure(44);
for nASP=1:48
    subplot(8,6,nASP);
    imagesc(Z(:,:,nASP));colormap(gray);
end
figure(45);
for nASP=1:48
    subplot(8,6,nASP);
    imagesc(W5(:,:,nASP));colormap(gray);
end
for nn=2:2:48
    W24(:,:,nn/2)=W5(:,:,nn)-W5(:,:,nn-1);
end
W24=sum(W5,3);


sumOrder = ...
         [13     15      17      19      21      23 ...
         1      3       5       7       9       11 ...
         2      4       6       8       10      12 ...
         14     16      18      20      22      24];
     
 for nk=1:24
     K(nk,:)=reshape(W24(:,:,sumOrder(nk)),1,[]);
 end
 
% 
% % Suren's new par to ASP layout sorting
% nfSort1=[47 35  5 43 13 19;
%           7 45  1 29 39 27;
%          31 17  9 21 15 33;
%          37 41 25  3 23 11;];
% nfSort2= nfSort1 + 1;  
% 
%    figure(32);
%    for mA=1:48
%        subplot(8,6,mA);
%        if(mod(mA,2)==1)
%        imagesc(squeeze(W5(:,:,2*sumOrder(nfSort1'==mA))));colormap(gray);
%        Gw_Meas_Impulse(:,:,mA)=W5(:,:,2*sumOrder(nfSort1'==mA));
%        else
%            imagesc(squeeze(W5(:,:,2*sumOrder(nfSort2'==mA)-1)));colormap(gray);
%                   Gw_Meas_Impulse(:,:,mA)=W5(:,:,2*sumOrder(nfSort2'==mA)-1);
%        end
%            
%    end
% 
%    save('Gw_Meas_Impulse','Gw_Meas_Impulse');