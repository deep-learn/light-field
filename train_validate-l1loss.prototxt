name: "LFReconNet"
layer {
  name: "data"
  type: "HDF5Data"
  top: "data"
  top: "label"
  hdf5_data_param {
    source: "examples/light-field/train.txt"
    batch_size: 128
  }
  include: { phase: TRAIN }
}
layer {
  name: "data"
  type: "HDF5Data"
  top: "data"
  top: "label"
  hdf5_data_param {
    source: "examples/light-field/test.txt"
    batch_size: 50
  }
  include: { phase: TEST }
}
layer {
  name: "fc1"
  type: "InnerProduct"
  # learning rate and decay multipliers for the weights
  param { lr_mult: 1 decay_mult: 1 }
  # learning rate and decay multipliers for the biases
  param { lr_mult: 2 decay_mult: 0 }
  inner_product_param {
    num_output: 3025
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
  bottom: "data"
  top: "fc1"
}
 layer {
    name: "reshape"
    type: "Reshape"
    bottom: "fc1"
    top: "reshape"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 1  #dimensions of data (c,x,y,u,v)
        dim: 11
        dim: 11 
        dim: 5
        dim: 5
      }
    }
  }
  
layer {
  name: "conv1"
  type: "Convolution"
  bottom: "reshape"
  top: "conv1"
  param {
    lr_mult: 1
  }
  param {
    lr_mult: 0.1
  }
  convolution_param {
    num_output: 64

    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3

    stride: 1

    pad: 1
    pad: 1
    pad: 1
    pad: 1
    engine: CAFFE
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}


layer {
    name: "r1"
    type: "Reshape"
    bottom: "conv1"
    top: "r1"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0 #dimensions of data (c,x,y,u,v)
        dim: 11
        dim: 275
      }
    }
  }
  




layer {
  name: "relu1"
  type: "ReLU"
  bottom: "r1"
  top: "r1"
}

layer {
    name: "r1end"
    type: "Reshape"
    bottom: "r1"
    top: "r1end"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0  #dimensions of data (c,x,y,u,v)
        dim: 11
        dim: 11
        dim: 5
        dim: 5
      }
    }
  }
  

layer {
  name: "conv2"
  type: "Convolution"
  bottom: "r1end"
  top: "conv2"
  param {
    lr_mult: 1
  }
  param {
    lr_mult: 0.1
  }
  convolution_param {
    num_output: 32

    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3

    stride: 1

    pad: 1
    pad: 1
    pad: 1
    pad: 1
    engine: CAFFE

    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}


layer {
    name: "r2"
    type: "Reshape"
    bottom: "conv2"
    top: "r2"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0 #dimensions of data (c,x,y,u,v)
        dim: 11
        dim: 275
      }
    }
  }
  




layer {
  name: "relu2"
  type: "ReLU"
  bottom: "r2"
  top: "r2"
}

layer {
    name: "r2end"
    type: "Reshape"
    bottom: "r2"
    top: "r2end"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0  #dimensions of data (c,x,y,u,v)
        dim: 11
        dim: 11
        dim: 5
        dim: 5
      }
    }
  }
  

layer {
  name: "conv3"
  type: "Convolution"
  bottom: "r2end"
  top: "conv3"
  param {
    lr_mult: 1
  }
  param {
    lr_mult: 0.1
  }
  convolution_param {
    num_output: 1

    kernel_size: 3
    kernel_size: 3
    kernel_size: 3
    kernel_size: 3

    stride: 1

    pad: 1
    pad: 1
    pad: 1
    pad: 1
    engine: CAFFE

    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
      value: 0
    }
  }
}


layer {
    name: "r3"
    type: "Reshape"
    bottom: "conv3"
    top: "r3"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0 #dimensions of data (c,x,y,u,v)
        dim: 1
        dim: 3025
      }
    }
  }
  




layer {
  name: "relu3"
  type: "ReLU"
  bottom: "r3"
  top: "r3"
}


layer {
    name: "rlabel"
    type: "Reshape"
    bottom: "label"
    top: "labelf"
    reshape_param {
      shape {
        dim: 0  # Number of examples 
        dim: 0 #dimensions of data (c,x,y,u,v)
        dim: 1
        dim: 3025
      }
    }
  }


#  layer {
#  name: "loss"
#  type: "EuclideanLoss"
#  bottom: "r3"
#  bottom: "label"
#  top: "loss"
#}
layer {
  name: 'loss'
  type: 'Python'
  bottom: 'r3'
  bottom: 'labelf'
  top: 'loss'
  python_param {
    # the module name -- usually the filename -- that needs to be in $PYTHONPATH
    module: 'l1loss'
    # the layer name -- the class name in the module
    layer: 'L1LossLayer'
  }
  # set loss weight so Caffe knows this is a loss layer
  loss_weight: 1
}
