""" Authors Initials: SJ/AJ
    This script generates compressed training/validation data by pre-processing lightfield dumps
"""
# Script to read python
import re
import cv2
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import scipy.linalg
import h5py
import math
import animateLightField as animlf
import os
from os.path import expanduser
import pdb

## Flags
animate = 1     # Enable input animation for 1st data only
create_data = 1 # Enable data creation

trainingLightFieldFilenames = ['fishi_5x5_ap35','messerschmitt_5x5_ap35','shrubbery_5x5_ap35', 'dice_5x5_ap35']

# number of training datasets
numTrainingLightFields = len(trainingLightFieldFilenames)

# size of a patch [spatial_y spatial_x angular_y angular_x]
patchSize = (11, 11, 5, 5)
lightFieldRes = (593,840,5,5)

# number of training patches
numTrainingPatches = 50000; 

# Preallocate TT
TT = np.zeros((numTrainingLightFields,1)+lightFieldRes)

#load all 4D training light fields
Path = os.path.dirname(os.path.realpath(__file__)) + '/CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'

for k in range(0,numTrainingLightFields):
    shortname = re.search('(.+?)_',trainingLightFieldFilenames[k]).group(1)
    print "Loading: ", shortname

    for ky in range(lightFieldRes[2]):
        for kx in range(lightFieldRes[3]):
            
            # Read image in grayscale
            I = cv2.imread(Path + trainingLightFieldFilenames[k] + '/' + shortname + '-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
            I = I.astype('float32')
            I = I-I.min()
            I = I/(I.max()-I.min())

            # Store values, [LF_indx,nColor,spatial_y,spatial_x,angular_y,angular_x]
            TT[k,0,:,:,ky,kx] = I
            
## animate
if animate == 1:
    # Changing the shape of TT to expected animated shape
    # expected [angular_y angular_x spatial_y spatial_x numColorChannels]
    # Just animating the first(fishy) data
    lf = np.transpose(TT[2,:,:,:,:,:],(3,4,1,2,0)); 
    print 'Animating the input: Close the figure to proceed'
    animlf.animateLightField(lf)

# now get random patches
Patches = np.zeros((numTrainingPatches,)+(1,)+patchSize)
# Seed to reproduce(Comment this out if different results required)
sd1 = 10
np.random.seed(sd1)
for k in range(numTrainingPatches):
    N = np.random.randint(0,numTrainingLightFields) #pick which LF
    y = np.random.randint(0,lightFieldRes[0]-patchSize[0]) #pick y
    x = np.random.randint(0,lightFieldRes[1]-patchSize[1]) #pick x
    Patches[k,0,:,:,:,:] = TT[N,0,y:y+patchSize[0],x:x+patchSize[1],:,:]

print Patches.shape
#plt.imshow(Patches[0,0,:,:,2,2],cmap=plt.cm.Greys_r)
#plt.show()

## animate first random patch
if animate == 1:
    # Changing the shape of Patches to expected animated shape
    # expected [angle_y angle_x spatial_y spatial_x numColorChannels]
    # Just animating the first(fishy) data
    lf = np.transpose(Patches[0,:,:,:,:,:],(3,4,1,2,0)); 
    print 'Animating first patch: Close the figure to proceed'
    animlf.animateLightField(lf)

# Simulate compressive random projections
# Seed to reproduce(Comment this out if different results required)
if create_data == 1:
    sd2 = 2
    np.random.seed(sd2)
    M = 800
    Phi = np.random.random((M,np.prod(patchSize)))
    #print Phi.max()
    #print Phi.min()
    Phi = Phi.T
    Phi = scipy.linalg.orth(Phi)
    Phi = Phi.T
    
    #print Phi.max()
    #print Phi.min()
    #print np.dot(Phi,Phi.T)
    
    # Dump the phi matrix to evaluate caffemodel later
    np.savez('phiMat-seed%d.npz' % sd2, phi=Phi)
    
    print 'Phi dimension: ', Phi.shape
    Measurements = np.zeros((numTrainingPatches,1,M))
    for k in range(0,numTrainingPatches):
        x = np.ravel(Patches[k,0,:,:,:,:])
        #print x.shape
        y = np.dot(Phi,x)
        #print y.shape
        Measurements[k,0,:] = y
    
    
    
    
    # split into train and validation (must be called data, label)
    split = int(math.floor(numTrainingPatches*0.85))
    label = np.zeros((split,1,patchSize[0]*patchSize[1]*patchSize[2]*patchSize[3]),dtype='float32')
    label_val = np.zeros((numTrainingPatches-split,1,patchSize[0]*patchSize[1]*patchSize[2]*patchSize[3]),dtype='float32')
    data = np.zeros((split,1,M),dtype='float32')
    data_val = np.zeros((numTrainingPatches - split,1,M),dtype='float32')
    # size tests
    print 'labelshape = ' +str(label.shape)
    print 'valshape = ' + str(label_val.shape)
    
    for k in range(0,numTrainingPatches):
        if k >= split:
            data_val[k-split,0,:] = Measurements[k,0,:]
            label_val[k-split,0,:] = np.ravel(Patches[k,0,:,:,:,:])
        else:
            data[k,0,:] = Measurements[k,0,:]
            label[k,0,:] = np.ravel(Patches[k,0,:,:,:,:])
    
    
    #create hdf5 formats compatible with Caffe
    tfn = "train-Randphi-%d-%d.h5"%(sd1,sd2)
    vfn = "val-Randphi-%d-%d.h5"%(sd1,sd2)
    f  = h5py.File(tfn,"w")
    f['data'] = data.astype('float32')
    f['label'] = label.astype('float32')
    f.close()
    
    f2 = h5py.File(vfn,"w")
    f2['data'] = data_val.astype('float32')
    f2['label'] = label_val.astype('float32')
    f2.close()
    print "Dumped: %s, %s at %s" % (tfn,vfn,os.getcwd())
else:
    print 'Enable data_create flag to generate train/val h5 files'
