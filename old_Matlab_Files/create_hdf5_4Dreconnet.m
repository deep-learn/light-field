clear all
Patch_dataset = zeros(200000,1,11,11,5,5);
microlens_dataset = zeros(200000,81);
for i = 1:4
data_file = ['/home/kkulkar1/4DReconnet/Datasets/Microlens_data/Data',num2str(i),'.mat'];
load(data_file);
microlens_dataset((i-1)*50000+1:i*50000,:) = microlens; 
Patch_dataset((i-1)*50000+1:i*50000,:,:,:,:,:) = Patch; 
end
clear Patch microlens

savepath_train = '/home/kkulkar1/4DReconnet/Datasets/train.h5';
savepath_val = '/home/kkulkar1/4DReconnet/Datasets/val.h5';

rand('seed',0);
order = randperm(200000);
Patch_train = Patch_dataset(1:0.8*200000,:,:,:,:,:);
microlens_train = microlens_dataset(1:0.8*200000,:);
Patch_val = Patch_dataset(0.8*200000+1:0.85*200000,:,:,:,:,:);
microlens_val =  microlens_dataset(0.8*200000:0.85*200000,:);

Patch_train = permute(Patch_train,[6,5,4,3,2,1]);
microlens_train = permute(microlens_train,[2,1]);
Patch_val = permute(Patch_val,[6,5,4,3,2,1]);
microlens_val = permute(microlens_val,[2,1]);



rand('seed',1);
order_train = randperm(0.8*200000);
Patch_train = Patch_train(:,:,:,:,:,order_train);
Patch_train = reshape(Patch_train,[3025 1 1 0.8*200000]);
microlens_train = microlens_train(:,order_train);

rand('seed',2);
order_val =   randperm(0.05*200000);
Patch_val = Patch_val(:,:,:,:,:,order_val);
Patch_val = reshape(Patch_val,[3025 1 1 0.05*200000]);
microlens_val = microlens_val(:,order_val);

count_train = 0.8*200000;
count_val = 0.05*200000;



% 
% %% writing to HDF5
 chunksz = 128;
 created_flag = false;
 totalct = 0;
 
for batchno = 1:floor(count_train/chunksz)
     last_read=(batchno-1)*chunksz;
     batchdata = microlens_train(:,last_read+1:last_read+chunksz); 
     batchlabs = Patch_train(:,:,:,last_read+1:last_read+chunksz);
     startloc = struct('dat',[1,totalct+1], 'lab', [1,1,1,totalct+1]);
     curr_dat_sz = store2hdf5(savepath_train, batchdata, batchlabs, ~created_flag, startloc, chunksz); 
     created_flag = true;
     totalct = curr_dat_sz(end);
 end
 h5disp(savepath_train);


 chunksz = 40;
 created_flag = false;
 totalct = 0;
 
for batchno = 1:floor(count_val/chunksz)
     last_read=(batchno-1)*chunksz;
     batchdata = microlens_val(:,last_read+1:last_read+chunksz); 
     batchlabs = Patch_val(:,:,:,last_read+1:last_read+chunksz);
     startloc = struct('dat',[1,totalct+1], 'lab', [1,1,1,totalct+1]);
     curr_dat_sz = store2hdf5(savepath_val, batchdata, batchlabs, ~created_flag, startloc, chunksz); 
     created_flag = true;
     totalct = curr_dat_sz(end);
 end
 h5disp(savepath_val);
                
                
                
           
   

