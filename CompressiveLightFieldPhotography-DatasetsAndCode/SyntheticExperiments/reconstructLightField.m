%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Load a 4D test light field from file, simulate coded projection onto a
%   2D sensor image, and recover the 4D light field. 
%
%   Please note that this simulation does not exactly match physically
%   realizable constraints. For each sensor pixel, all incident angles of
%   the light field are randomly modulated and summed. The optical setup
%   described in the SIGGRAPH 2013 paper "Compressive Light Field
%   Photography using Overcomplete Dictionaries and Optimized Projections"
%   uses a coded attenuation mask in between sensor and camera aperture. As
%   such, light field angles are averaged, but the mask code is not random
%   but periodic over the sensor image. The setup simulated in this code is
%   not exactly what we used in the paper, but more more intuitive and
%   easier to use.
%
%   Gordon Wetzstein [gordonw@media.mit.edu]
%   Research Scientist
%   MIT Media Lab . Camera Culture Group
%   media.mit.edu/~gordonw
%   July 2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clf;

addpath('../3rdparty/spgl1-1.7');
addpath('../3rdparty/');

% recover only a small test region
bTestRegion = false;

% 0 is patch-by-patch (faster), 1 is sliding patches (much slower)
patchMode = 0;

% load dictionary
load('DictionaryLearning/LightField4D-Dictionary_9x9.mat'); % this is a better dictionary 
%load('DictionaryLearning/LightField4D-Dictionary.mat'); % use your own dictionary learned with KSVD

% max iterations for optimization
maxIters = 100*patchSize(3)*patchSize(4);

% draw intermediate results
bDraw = false;

% print progress and current error
echoProgress = true;

% grayscale (faster)
bGrayscale = true;

% solver:
%   0 - SPGL1
%   1 - ADMM
bReconstructionMethod = 1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% load original light field

% number of pixels in [angle_y angle_x spatial_y spatial_x numColorChannels]
lightFieldResolution = [5 5 593 840 3];
lightField = zeros(lightFieldResolution);

for ky=1:lightFieldResolution(1)
    for kx=1:lightFieldResolution(2)
        I = im2double(imread(sprintf('DragonsAndBunnies_5x5_ap6.6/dragons-%.2d.png', (ky-1)*lightFieldResolution(2)+kx)));
        lightField(ky,kx,:,:,:) = reshape(I, [1 1 lightFieldResolution(3) lightFieldResolution(4) 3]);
    end
end

numColorChannels = 3;
if bGrayscale
    lightField = mean(lightField,5);    
    lightFieldResolution = size(lightField);
    numColorChannels = 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% small region
if bTestRegion 
    lightField = lightField(:,:,193:232,473:512,:);    
    lightFieldResolution = size(lightField);
end

if bDraw
    imgL = drawLightField4D(lightField);
    imgL = imresize(imgL, [lightFieldResolution(3) lightFieldResolution(4)]);
    subplot(2,2,1); imshow(imgL); title('Target Light Field');
    if numColorChannels == 1
        imgL = reshape(lightField( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :), [lightFieldResolution(3) lightFieldResolution(4)]);
    else
        imgL = reshape(lightField( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :, :), [lightFieldResolution(3) lightFieldResolution(4) 3]);
    end
    subplot(2,2,3); imshow(imgL); title('Target Light Field - Central View');
    clear imgL;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% patch-by-patch reconstruction (fast)

if patchMode == 0

    numImagePatches = [floor(lightFieldResolution(3)/patchSize(3)) floor(lightFieldResolution(4)/patchSize(4))];

    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);

    % reconstruct all the patches
    totalTime = tic;
%     for ii=1:numColorChannels*numImagePatches(1)*numImagePatches(2)        
%         
%         % current color channel
%         c  = floor( (ii-1) / (numImagePatches(1)*numImagePatches(2)) ) + 1;
%         % current patch index in y
%         ky = floor( ((ii - (c-1)*numImagePatches(1)*numImagePatches(2))-1) / numImagePatches(2) ) + 1; 
%         % current patc hindex in x
%         kx = mod(ii-1,numImagePatches(2))+1;
        
	for c=1:numColorChannels
    	for ky=1:numImagePatches(1)
        	for kx=1:numImagePatches(2)
        
        %disp([num2str(kx) ', ' num2str(ky) ', ' num2str(c)]);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % original patch
        Pori = lightField(:,:, (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4), c);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % project down to 2D measurement

        % randome values for averaging light field angles               
        %R = rand(patchSize);                                
        R = (randn(patchSize)./2)+0.5; R(R<0)=0; R(R>1)=1;                                
        R = R(:);

        % construct measurement matrix from random angle averaging
        PHI = zeros([patchSize(3)*patchSize(4) prod(patchSize)]);
        count = 1;
        for kky=1:patchSize(3)

            for kkx=1:patchSize(4)
                PHI( (kky-1)*patchSize(3)+kkx, count:count+patchSize(1)*patchSize(2)-1 ) = R(count:count+patchSize(1)*patchSize(2)-1);
                count = count + patchSize(1)*patchSize(2);
            end
        end               

        % measurement on sensor 
        I = PHI*Pori(:);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % reconstruct patch with SPGL1

        if bReconstructionMethod == 0

            sigma = 0.001;    % Desired ||Ax - b||_2

            % options
            opts = spgSetParms( 'verbosity', 0,...
                                'iterations', maxIters );

            % reconstruct sparse coefficients
            currentTime = tic;
            alpha   = spg_bpdn(PHI*D, I, sigma, opts);
            timeElapsed = toc(currentTime);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % reconstruct patch with ADMM

        else

            lambda = 0.01;
            currentTime = tic;
            alpha = lassoADMM(PHI*D, I, lambda, 1.0, 1.0);
            timeElapsed = toc(currentTime);

        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % reconstruct patch
        Prec    = D*alpha;

        % plot progress and error
        if echoProgress
            MSR  = mean( (Pori(:)-Prec).^2 );
            PSNR = 10 * log10( 1/MSR );
            disp(['patch ' num2str( (c-1)*numImagePatches(2)*numImagePatches(1) + (ky-1)*numImagePatches(2) + kx) ' of ' num2str(numImagePatches(1)*numImagePatches(2)*numColorChannels) ', took (in ms): ' num2str(timeElapsed) ', PSNR (in dB): ' num2str(PSNR)]);
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % sort into reconstruction
        lightFieldRec(:,:, (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4), c) = reshape(Prec, patchSize);

        % plot result
        if bDraw
            imgL = drawLightField4D(lightFieldRec);
            imgL = imresize(imgL, [lightFieldResolution(3) lightFieldResolution(4)]);
            subplot(2,2,2); imshow(imgL); title('Reconstructed Light Field');
            if numColorChannels == 1
                imgL = reshape(lightFieldRec( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :), [lightFieldResolution(3) lightFieldResolution(4)]);
            else
                imgL = reshape(lightFieldRec( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :, :), [lightFieldResolution(3) lightFieldResolution(4) 3]);
            end
            subplot(2,2,4); imshow(imgL); title('Reconstructed Light Field - Central View');
            clear imgL;
            drawnow;
            
            % show animation of reconstructed light field
            animateLightField(lightFieldRec, 2);
        end
            
            end
        end
    end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
    % save data
    save('LightField-Reconstruction-PatchByPatch.mat', 'lightField', 'lightFieldRec', 'patchSize');
    
else

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% sliding patch reconstruction (very slow)

    % initialize reconstructed light field
    lightFieldRec = zeros(lightFieldResolution);

    numImagePatches = [lightFieldResolution(3)-patchSize(3) lightFieldResolution(4)-patchSize(4)];
    
    % reconstruct all the patches
    totalTime = tic;
    for ii=1:numColorChannels*numImagePatches(1)*numImagePatches(2)
        
%    for c=1:numColorChannels
%        for ky=1:lightFieldResolution(3)-patchSize(3)
%            for kx=1:lightFieldResolution(4)-patchSize(4)

        % current color channel
        c  = floor( (ii-1) / (numImagePatches(1)*numImagePatches(2)) ) + 1;
        % current patch index in y
        ky = floor( ((ii - (c-1)*numImagePatches(1)*numImagePatches(2))-1) / numImagePatches(2) ) + 1; 
        % current patc hindex in x
        kx = mod(ii-1,numImagePatches(2))+1;

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % original patch
                Pori = lightField(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c);

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % project down to 2D measurement

                % randome values for averaging light field angles               
                R = rand(patchSize);                                
                R = R(:);
                
                % construct measurement matrix from random angle averaging
                PHI = zeros([patchSize(3)*patchSize(4) prod(patchSize)]);
                count = 1;
                for kky=1:patchSize(3)
                    
                    for kkx=1:patchSize(4)
                        PHI( (kky-1)*patchSize(3)+kkx, count:count+patchSize(1)*patchSize(2)-1 ) = R(count:count+patchSize(1)*patchSize(2)-1);
                        count = count + patchSize(1)*patchSize(2);
                    end
                end
                                
                % measurement on sensor 
                I = PHI*Pori(:);

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % reconstruct patch with SPGL1
                
                if bReconstructionMethod == 0

                    sigma = 0.01;    % Desired ||Ax - b||_2

                    % options
                    opts = spgSetParms( 'verbosity', 0,...
                                        'iterations', maxIters );

                    % reconstruct sparse coefficients
                    currentTime = tic;
                    alpha   = spg_bpdn(PHI*D, I, sigma, opts);
                    timeElapsed = toc(currentTime);                   
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % reconstruct patch with ADMM

                else

                    lambda = 0.01;
                    currentTime = tic;
                    alpha = lassoADMM(PHI*D, I, lambda, 1.0, 1.0);
                    timeElapsed = toc(currentTime);

                end
                
                % reconstruct patch
                Prec    = D*alpha;
                        
                % plot progress and error
                if echoProgress
                    MSR  = mean( (Pori(:)-Prec).^2 );
                    PSNR = 10 * log10( 1/MSR );
                    disp(['patch ' num2str( (c-1)*(lightFieldResolution(3)-patchSize(3))*(lightFieldResolution(4)-patchSize(4)) + (ky-1)*(lightFieldResolution(4)-patchSize(4)) + kx) ' of ' num2str((lightFieldResolution(3)-patchSize(3))*(lightFieldResolution(4)-patchSize(4))*numColorChannels) ', PSNR (in dB): ' num2str(PSNR)]);
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % sort into reconstruction
                lightFieldRec(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c) = lightFieldRec(:,:, ky:ky+patchSize(4)-1, kx:kx+patchSize(4)-1, c) + (1/(patchSize(3)*patchSize(4))).*reshape(Prec, patchSize);

                % plot result
                if bDraw
                    imgL = drawLightField4D(lightFieldRec);
                    imgL = imresize(imgL, [lightFieldResolution(3) lightFieldResolution(4)]);
                    subplot(2,2,2); imshow(imgL); title('Reconstructed Light Field');
                    if numColorChannels == 1
                        imgL = reshape(lightFieldRec( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :), [lightFieldResolution(3) lightFieldResolution(4)]);
                    else
                        imgL = reshape(lightFieldRec( floor(lightFieldResolution(1)/2)+1, floor(lightFieldResolution(2)/2)+1, :, :, :), [lightFieldResolution(3) lightFieldResolution(4) 3]);
                    end
                    subplot(2,2,4); imshow(imgL); title('Reconstructed Light Field - Central View');
                    clear imgL;
                    drawnow;
                end
%            end
%        end        
%        end                
    end
    elapsedTimeTotal = toc(totalTime);
    elapsedTimeTotal
    
    % save data
    save('LightField-Reconstruction-SlidingPatch.mat', 'lightField', 'lightFieldRec', 'patchSize');

    % show animation of reconstructed light field
    figure; animateLightField(lightFieldRec, 2);
    
end
    
