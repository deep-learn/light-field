The folders contains scripts and synthetic (rendered) datasets 
that you can use to test compressive light field photography. 

The directory subfolder contain light fields that are used as 
a training set for learning a dictionary of light field atoms.
See the subfolder for more details.

This folder contains the helper scripts 'animateLightFields.m' 
and 'drawLightField4D.m' as well as the main script 
'reconstructLightField.m'. The latter loads a test light 
included in the folder 'DragonsAndBunnies_5x5_ap6.6', and 
simulates randomly coded projection of the 4D light field onto a 
2D sensor image. Then a reconstruction is performed using the 
SPGL1 toolbox.

Please note that this code serves only as example code that is 
intuitive and easy to run. In our SIGGRAPH 2013 paper "Compressive 
Light Field Photography with Overcomplete Dictionaries and 
Optimized Projections", we actually used a parallel implementation 
of a fast homotopy recovery algorithm that runs on Linux and 
requires many toolboxes and libraries to be installed. No results 
in the paper are actually generated using these scripts, because 
they are very slow and do not scale well to higher image resolutions.
However, these scripts demonstrate the principle of operation of 
compressive light field photography. 

The quality of results in the paper, especially the simulations, is 
sgnificantly better than those produced by these scripts. This is 
mainly due to the dictionary learning process. KSVD, as used in these
scripts, is limited by the number of training samples that can be used.
For our paper, we used significantly more training samples and a different
dictionary for the reconstructions. We are planning on releasing all
of that code as well in the future.


For questions and comments, please send me an email.


Gordon Wetzstein [gordonw@media.mit.edu]
Research Scientist
MIT Media Lab . Camera Culture Group
media.mit.edu/~gordonw
July 2013