%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Pick random 4D light field patches from different training light fields
%   and learn an overcomplete dictionary from them.
%
%   Gordon Wetzstein [gordonw@media.mit.edu]
%   Research Scientist
%   MIT Media Lab . Camera Culture Group
%   media.mit.edu/~gordonw
%   July 2013
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all;

addpath('../../3rdparty/ksvdbox13');
addpath('../../3rdparty/ompbox10');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% training light field names

trainingLightFieldFilenames = { ...
    'dice_5x5_ap35',
    'fishi_5x5_ap35',
    'messerschmitt_5x5_ap35',
    'shrubbery_5x5_ap35' };
    
% number of training datasets
numTrainingLightFields  = size(trainingLightFieldFilenames,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% size of a patch [angular_y angular_x spatial_y spatial_x]
patchSize               = [5 5 8 8];

% number of training patches
numTrainingPatches      = 40000;                % as many as possible

% dictionary size
dictSize                = prod(patchSize) * 1; % 1x overcomplete

% desired sparsity level of coefficients for dictionary
sparsityLevel           = 3;                    % seems like lower number takes much longer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate training patches randomly



% load all 4D training light fields
for k=1:numTrainingLightFields
    shortname_idx = findstr(trainingLightFieldFilenames{k}, '_');
    shortname = trainingLightFieldFilenames{k};
    shortname = shortname(1:shortname_idx(1)-1);
    
    for ky=1:5
        for kx=1:5
            
            % read current image
            idx = (ky-1)*5 + kx;            
            I = imread([ trainingLightFieldFilenames{k} '/' shortname '-' sprintf('%.2d',idx) '.png']);
            
            % sort into data structure            
            TT(k,ky,kx,:,:) = mean(I,3);
        end
    end
end


% select numTrainingPatches random training patches
randLF = round(rand([1 numTrainingPatches])*(numTrainingLightFields-1)) + 1;
randSY = round(rand([1 numTrainingPatches])*(size(TT,4)-1-patchSize(3))) + 1;
randSX = round(rand([1 numTrainingPatches])*(size(TT,5)-1-patchSize(4))) + 1;

params.data = zeros([prod(patchSize) numTrainingPatches]);
for k=1:numTrainingPatches
	currentPatch = TT(  randLF(k),:,:,...
                        randSY(k):randSY(k)+patchSize(3)-1,...
                        randSX(k):randSX(k)+patchSize(4)-1);                   
	params.data(:,k) = currentPatch(:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% set other parameters and learn dictionary

% learn dictionary
    
% set parameters
params.Tdata        = sparsityLevel;
params.dictsize     = dictSize;

% run KSVD
disp('Learning dictionary (this may take a while)');
D = ksvd(params);

% add DC term to dictionary
D = [D 255.*ones([prod(patchSize) 1])];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% save dictionary
disp('Done. Saving dictionary as file: LightField4D-Dictionary.mat');
filename = 'LightField4D-Dictionary.mat';
save(filename, 'D', 'patchSize');

% normalize atoms and show dictionaries 
%subplot(1,2,2)
kk = floor(sqrt(params.dictsize));
DD = zeros([kk kk] .* [patchSize(3) patchSize(4)]);
k=1;
for ky=1:kk
    for kx=1:kk
        currentPatch = reshape(D(:,k), patchSize);
        currentPatch = reshape(currentPatch(3,3,:,:), [patchSize(3) patchSize(4)]);
        currentPatch = (currentPatch-min(currentPatch(:))) / (max(currentPatch(:))-min(currentPatch(:)));
        DD( (ky-1)*patchSize(3)+1:ky*patchSize(3), (kx-1)*patchSize(4)+1:kx*patchSize(4) ) = ...
            currentPatch;
        k = k+1;
    end
end
imshow(DD);
title('Central Views of Learned Light Field Atoms');

