The subfolders in this directory contain light fields that are
rendered with povray and used as a training set for learning 
a dictionary of light field atoms.

Run the script 'learnDictionary.m' to learn it with your own 
parameters. The dictionary will be saved in the file
'LightField4D-Dictionary.mat' and is required for sparse light 
field recovery.

For convenience, I included a dictionary that is learned using 
the standard parameters. 

Please note that this code serves only as example code that is 
intuitive and easy to run. In our SIGGRAPH 2013 paper "Compressive 
Light Field Photography with Overcomplete Dictionaries and 
Optimized Projections", we actually used coresets and different, 
larger-scale optimization techniques to learn a dictionary from 
more than 1,000,000 training patches (as opposed to 40,000 in 
this example code). 

A dictionary learned with that technique is also included in the 
file 'LightField4D-Dictionary_9x9.mat'. This should perform 
significantly better than the dictionaries learned with the 
included KSVD code because we used approx. 100x more training
samples for the dictionary learning.


For questions and comments, please send me an email.


Gordon Wetzstein [gordonw@media.mit.edu]
Research Scientist
MIT Media Lab . Camera Culture Group
media.mit.edu/~gordonw
July 2013