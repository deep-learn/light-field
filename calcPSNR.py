import numpy as np

def calcPSNR(lfo, lfr, patchSize):
    # Take care of unprocessed boundary pixels
    endy = lfo.shape[1] - lfo.shape[1]%patchSize[0]
    endx = lfo.shape[2] - lfo.shape[2]%patchSize[1]

    e = lfo[:,0:endy,0:endx,:,:]-lfr[:,0:endy,0:endx,:,:]
    return 20*np.log10(1/np.sqrt(np.mean(np.power(e,2))))
