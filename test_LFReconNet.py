""" This script 
    1 -> reads test data 
    2 -> compress it
    3 -> do forward pass
    4 -> stitch the output together for reconstruction
"""
# Suren Jayasuriya
# 
# Script to load up a network and test it on a sample lightfield
import sys
import numpy as np
import scipy
import subprocess
import math
import cv2
import os
import animateLightField as animLF
import caffe
import calcPSNR

# Flags
verbose = 0         # Verbose mode
vverbose = 0        # Extra verbose mode
animate = 0         # Enable animatation of input and output
saveanim = True    # Enable to save the animation

# Make sure caffe is on the python path
caffe_root = '../../'
sys.path.append(caffe_root)
sys.path.append(caffe_root + 'python')

#check status quo
print "Python: ", sys.version.split("\n")[0]
print "CUDA: ", subprocess.Popen(["nvcc","--version"],stdout=subprocess.PIPE).communicate()[0].split("\n")[3]

# set paths to model, weights
Model_file = 'deploy.prototxt'
#Weights = '../../LFrecon_gray_iter_15000.caffemodel'
Weights = 'ASP_sims_Diag_N6_iter_6000.caffemodel'

## Set up GPU
caffe.set_device(0)
caffe.set_mode_gpu()

#load net
net = caffe.Net(Model_file,Weights,caffe.TEST)

# INFO: Blobs are memory abstraction objects (with execution depending on the mode), and data is contained in the field data as an array
# INFO: net object contains 2 ordered dictionary: blobs and params
# INFO: net.blobs for input data and its propagation in the layers
# INFO: net.params a vector of blobs for weight and bias parameters
if vverbose == 1:
    print 'Blobs size:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]

# load test light field
patchSize = [11,11,5,5]
lightFieldRes = [593,840,5,5]

numImgPatches = [int(math.floor(lightFieldRes[0]/patchSize[0])),int(math.floor(lightFieldRes[1]/patchSize[1]))]
if verbose == 1:
    print "Number of Patches: ", numImgPatches

lightField = np.zeros((1,593,840,5,5))
lightFieldRec = np.zeros((1,593,840,5,5))

# Load all the images and normalize them
for ky in range(0,5):
    for kx in range(0,5):
        # read current image
        I = cv2.imread(os.getcwd() + '/../../../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DragonsAndBunnies_5x5_ap6.6/dragons-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
        I = I.astype('float32')
        I = I - I.min()
        I = I/(I.max()-I.min())

        lightField[0,:,:,ky,kx] = I

# (TEST) Animated the loaded lightfield
if animate == 1 or saveanim == 1:
    lf = np.transpose(lightField,(3,4,1,2,0))
    print 'Animating the input: Close the figure to proceed'
    animLF.animateLightField(lf,'LFOriginal.mp4',saveanim)

# Load Phi matrix
Phi = np.load('LFNetData/phiMat-seed2.npz')
Phi = Phi['phi']
x = np.zeros(patchSize)
if vverbose == 1:
    print Phi.shape
    print x.shape
    print lightField.shape

# Create patches, send it through the network, and place it in reconstructed light field
for ky in range(0,numImgPatches[0]):
    for kx in range(0,numImgPatches[1]):
        if verbose == 1:
            print 'Processing: ', (ky,kx)

        x = lightField[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:]
        x = np.ravel(x)
        measurement = np.dot(Phi,x)

        ## Change image dimension to match the data blob
        patch_input = measurement[np.newaxis, np.newaxis, :, np.newaxis]
        if vverbose == 1:
            print patch_input.shape
        net.blobs['data'].reshape(*patch_input.shape)
        net.blobs['data'].data[...] = patch_input
        
        ## Call Caffe to Run a forward pass
        net.forward()
        
        # Reshape output into patchSize
        lightFieldRec[0,ky*patchSize[0]:(ky+1)*patchSize[0],kx*patchSize[1]:(kx+1)*patchSize[1],:,:] = np.reshape(net.blobs['r3'].data, patchSize)

if vverbose == 1:
    print 'After:'
    print net.inputs
    print [(k, v.data.shape) for k, v in net.blobs.items()]
    print [(k, v[0].data.shape, v[1].data.shape) for k, v in net.params.items()]
    
    print 'Output:'
    print net.blobs['r3'].data.shape

PSNR = calcPSNR.calcPSNR(lightField, lightFieldRec, patchSize)
print "PSNR: %.2fdB" % (PSNR)
if animate == 1 or saveanim == 1:
    # (TEST) Animate the reconstructed lightfield
    lf = np.transpose(lightFieldRec,(3,4,1,2,0))
    print 'Animating the reconstruction: Close the figure to exit'
    animLF.animateLightField(lf,Weights+'recon-%.2fdB.mp4' % (PSNR),saveanim)

print "DONE"
