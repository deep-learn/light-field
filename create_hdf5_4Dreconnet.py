import numpy as np
import scipy.io as sio
import random
import h5py

## array for patch dataset
Patch_dataset = np.zeros((200000,1,11,11,5,5));
print(Patch_dataset.shape);

## array for microlens dataset
microlens_dataset = np.zeros((200000,81));

for i in range(1,5):
    data_file = '/home/molnargroup/Documents/reconnet/Data' + str(i) + '.mat';
    print(data_file);
    ## open file to read
    tmp = sio.loadmat(data_file);
    microlens_dataset[(i-1)*50000:i*50000,:] = tmp['microlens'];
    Patch_dataset[(i-1)*50000:i*50000,:,:,:,:,:] = tmp['Patch'];
## Clear/free tmp
del tmp;

## Initialize seed
random.seed(0);
order = np.random.permutation(200000);  ## FIXME

## training data
trainSz = 0.8;
Patch_train = Patch_dataset[0:trainSz*200000,:,:,:,:,:];
microlens_train = microlens_dataset[0:trainSz*200000,:];

## validation set
validSz = 0.05;
Patch_val = Patch_dataset[trainSz*200000:(trainSz+validSz)*200000,:,:,:,:,:];
microlens_val = microlens_dataset[trainSz*200000:(trainSz+validSz)*200000,:];

## Transpose both the sets
Patch_train = np.transpose(Patch_train);
microlens_train = np.transpose(microlens_train);
Patch_val = np.transpose(Patch_val);
microlens_val = np.transpose(microlens_val);

## Initialize seed and randomize train set
random.seed(1);
order_train = np.random.permutation(int(trainSz*200000));
Patch_train = Patch_train[:,:,:,:,:,order_train];
Patch_train = Patch_train.reshape(3025,1,1,trainSz*200000);
microlens_train = microlens_train[:,order_train];

## Initialize seed and randomize validation set
random.seed(2);
order_val = np.random.permutation(int(validSz*200000));
Patch_val = Patch_val[:,:,:,:,:,order_val];
Patch_val = Patch_val.reshape(3025,1,1,validSz*200000);
microlens_val = microlens_val[:,order_val];
print(microlens_train.shape);
print(Patch_train.shape);

count_train = trainSz*200000;
count_val = validSz*200000;

## Writing to HDF5
chunksz = 128;
created_flag = False;
totalct = 0;

savepath_train = '/home/molnargroup/Documents/reconnet/train.h5'

t = h5py.File(savepath_train, 'w');
t.create_dataset('data', data=microlens_train, chunks=(81,chunksz));
t.create_dataset('label', data=Patch_train, chunks=(3025,1,1,chunksz));
t.close()

chunksz = 40;
savepath_val = '/home/molnargroup/Documents/reconnet/val.h5'

v = h5py.File(savepath_val, 'w');
v.create_dataset('data', data=microlens_val, chunks=(81,chunksz));
v.create_dataset('label', data=Patch_val, chunks=(3025,1,1,chunksz));
v.close()
