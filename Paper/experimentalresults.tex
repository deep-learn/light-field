In this section, we show experimental results on both simulated light fields, real light fields with simulated capture, and finally real data taken from a prototype ASP camera~\cite{asp}. We compare both visual quality and reconstruction time for our reconstructions, and compare against baselines for each dataset. 

\subsection{Synthetic Experiments}
We first show simulation results on the Synthetic Light Field Archive. We used as our baseline the dictionary-based method from~\cite{marwah2013compressive, asp} with the dictionary trained on synthetic light fields, and we use the dragon scene as our test case. We utilize three types of $\Phi$ matrices, a random $\Phi$ matrix that represents the ideal 4D random projections matrix (satisfying RIP~\cite{candes2008restricted}), but is not physically realizable in hardware (rays are arbitrarily summed from different parts of the image sensor array). We also use $\Phi$ for coded masks placed in the body of the light field camera, a repeated random code that is periodically shifted in angle across the array. Finally, we use the $\Phi$ matrix for ASPs which consists of 2D oriented sinusoidal responses to angle. As you can see in Figure~\ref{fig:Phicompare}, the ASPs and the Mask reconstructions perform slightly better than the ideal random projections, perhaps since the compression ratio is low at $8\%$, which might not satisfy the limits of the compressed sensing theory. The reconstructions do suffer from blurred details in the zoomed inlets, which means that there is still spatial resolution that is not recovered by the network. 

\begin{figure*}
\includegraphics[height=6cm]{Figures/Phicompare.pdf}
\caption{The figure compares the reconstructions for the dragons scene for different encoding schemes, ASP, Mask and Ideal Random 4D projections (CS) using the two-branch network. We obtain better quality reconstruction results for ASP ()26.62 dB) as compared to Mask (25.46 dB) and CS (24.75 dB). }
\label{fig:Phicompare}
\end{figure*}

\textbf{Compression ratio} is the ratio of independent coded light field measurements to angular samples to reconstruct in the light field for each pixel. This directly corresponds to the number of rows in the $\Phi$ matrix which correspond to one pixel. We show a sweep of the compression ratio and measure the PSNR for both the mask and ASP light field cameras, as you can see in Figure~\ref{fig:compressionsweep}. The PSNR degrades gracefully as the number of measurements becomes smaller, with ASPs and coded mask cameras still achieving above 25 dB at a compression ratio of $8\%$. 

\begin{figure}
\includegraphics[width = \columnwidth]{Figures/compressionsweep.pdf}
\caption{The figure shows the variation of PSNR of reconstructions with the number of measurements for dragons scene for ASP and Mask using the two-stream network.
}
\label{fig:compressionsweep}
\end{figure}

\textbf{Noise:} We also tested the robustness of the networks to additive noise in the input images. We simulated Gaussian noise of standard deviation of 0.2 and 0.4, and record the PSNR and reconstruction time which is display in Figure~\ref{fig:noisesweep}. Note that the dictionary-based algorithm takes longer to process noisy patches due to its iterative $\ell_1$ solver, while our network has the same flat run time regardless fo the noise level. This is a distinct advantage of neural network-based methods over the iterative solvers.  The network also seems resilient to noise in general, as our PSNR remained about 26 dB.
\begin{table}
\begin{center}
    \begin{tabular}{||c|c|c|c|c||}
        \hline
        \textbf{Metrics} & \textbf{Noiseless} & \textbf{Std 0.1} & \textbf{Std 0.2}\\
        \hline \hline
        PSNR (Ours) & 26.77 & 26.74 & 26.66 \\
        \hline
        PSNR (Dictionary) & 25.80 & 21.98 & 17.40 \\


        \hline
        Time (Ours) & 242 & 242 & 242 \\
        \hline
        Time (Dictionary) & 3786 & 9540 & 20549 \\
        \hline
    \end{tabular}
\end{center}
\caption{The table shows how PSNR varies for different levels of additive Gaussian noise. It is clear that our method is extremely robust to high levels of noise and provides high PSNR reconstructions, while for the dictionary method, the quality of the reconstructions dip sharply with noise. Also shown is the time taken to perform the reconstruction. For our method, the time taken is only 242 s whereas for dictionary learning method, it can vary from 1 hour to nearly 7 hours.}
\label{fig:noisesweep}
\end{table}

\textbf{Lytro Illum Light Fields Dataset:} We show our results on the UCSD dataset in Figure~\ref{fig:ucsd}. As a baseline, we compare against the method from Kalantari \etal~\cite{LearningViewSynthesis} which utilize 4 input views from the light field and generate the missing angular viewpoints with a neural network. Our network model achieves higher PSNR values of 28-29 dB on these real light fields. While Kalantari \etal method achieves PSNR > 30dB on this dataset, this is starting from a 4D light field captured by the Lytro camera and thus does not have to uncompress coded measurements. Their effective compression ratio is $16\%$ without any encoding, while our network is processing compression ratios of $8\%$ with encoding. Our method is also slightly faster as their network takes 147 seconds to reconstruct the full light field, while our method reconstructs a light field in 80 seconds (both on a Titan X GPU). 

\begin{figure}
\includegraphics[width=\columnwidth]{Figures/ucsd.pdf}
\caption{UCSD Dataset reconstruction comparison. SNR is computed only for the central 5x5 views }
\label{fig:ucsd}
\end{figure}

%\subsubsection{Reconstruction Times}
%
%To compare reconstruction times, we benchmark the dictionary-based method and our method on the same CPU. The dictionary-based reconstruction is not available on a GPU, but for our neural network, we can reconstruct a light field on a TItan X in ZZZ seconds. See Figure~\ref{fig:time} for a list of timings for the two methods. 

%\begin{figure}
%\includegraphics[width=\columnwidth]{Figures/time.pdf}
%\caption{Time comparison }
%\label{fig:time}
%\end{figure}

\subsection{Real Experiments}

Finally, to show the feasibility of our method on a real compressive light field camera, we use data collected from a prototype ASP camera~\cite{asp}. This data was collected on an indoors scene, and utilized three color filters to capture color light fields of size (384,384,3). 

Since we don't have training data for these scenes, we train our two branch network on synthetic data, and then apply a linear scaling factor to ensure the testing data has the same statistics as the training data. We also change our $\Phi$ matrix to match the actual sensors response and measure the angular variation in our synthetic light fields to what we expect from the real light field. See Figure~\ref{fig:realexp} for our reconstructions. We compare our reconstructions against the method from Hirsch \etal~\cite{asp} which uses dictionary-based learning to reconstruct the light fields. For all reconstruction techniques, we apply post-processing filtering to the image to remove periodic artifacts due to the patch-based processing and non-uniformities in the ASP tile, as done in~\cite{asp}. 

\begin{figure*}[h!]
\includegraphics[height = 8cm]{Figures/realexp.pdf}
\caption{The figure shows the reconstructions for the real data from the ASP measurements using our method (for stride 5 and stride 2) and dictionary method (for stride 5), and the corresponding time taken. It is clear that the quality of the reconstructions for our method is comparable as that using the dictionary learning method, although the time taken for our method (94 seconds) is an order less than that for the dictionary learning method (35 minutes). }
\label{fig:realexp}
\end{figure*}
We first show the effects of stride for overlapping patch reconstructions for the light fields, as shown in Figure~\ref{fig:overlap}. Our network model takes a longer time to process smaller stride, but improves the visual quality of the results. This is a useful tradeoff between visual quality of results and reconstruction time in general. 

As you can see, the visual quality of the reconstructed scenes from the network are on-par with the dictionary-based method, but with an order of magnitude faster reconstruction times. A full color light field with stride of 5 in overlapping patches can be reconstructed in 40 seconds, while an improved stride of 2 in overlapping patches yields higher quality reconstructions for 6.7 minutes of reconstruction time. The dictionary-based method in contrast takes 35 minutes for a stride of 5 to process these light fields. The recovered light fields have parallax, although some distortions exist that may be due to optical abberations, a mismatch between the real $\Phi$ response and the model $\Phi$, and higher noise in the real data as compared to synthetic data. However, we believe these results represent the potential for using neural networks to recover 4D light fields from real coded light field cameras. 

\begin{figure}
\includegraphics[width=\columnwidth]{Figures/overlap.pdf}
\caption{Comparison of non-overlapping patches and overlapping patches with strides of 11 (non-overlapping), 5, and 2.}
\label{fig:overlap}
\end{figure}



