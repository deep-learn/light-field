\begin{figure}[t]
\begin{center}
%\fbox{\rule{0pt}{2in} \rule{0.9\linewidth}{0pt}}
 \includegraphics[width=0.8\linewidth]{Figures/Lightfieldconcept.pdf}
\end{center}
   \caption{This figure shows the process of light-field capture with various types of light field cameras and setups.}
\label{lightfieldconcept}
\end{figure}

In this section, we describe the image formation model for capturing 4D light fields and how to reconstruct them. 

A 4D light field is typically parameterised with either a two plane or two angular representation~\cite{levoy1996light, gortler1996lumigraph}. We will represent light fields $l(x,y,\theta,\phi)$ with two spatial coordinates and two angular coordinates. For a regular image sensor, the angular coordinates for the light field are integrated over the main lens, thus yielding the following equation:
\begin{equation}
i(x,y) = \int_{\theta} \int_{\phi} l(x,y,\theta,\phi) d\phi d\theta,
\end{equation}
where $i(x,y)$ is the image and $l(x,y,\theta,\phi)$ is the light field. 

Single-shot light field cameras add a modulation function $\Phi(x,y,\theta,\phi)$ that weights the incoming rays~\cite{Wetzstein:PlenopticMultiplexing:2012}:

\begin{equation}
i(x,y) = \int_{\theta} \int_{\phi} \Phi(x,y,\theta,\phi) \cdot l(x,y,\theta,\phi) d\phi d\theta.
\end{equation}
When we vectorize this equation, we get $\vec{i} = \Phi\vec{l}$ where the $\vec{l}$ is the vectorized light field, $\vec{i}$ is the vectorized image, and $\Phi$ is the matrix discretizing the modulation function. Since light fields are 4D and images are 2D, this is inherently an underdetermined set of equations where $\Phi$ has more columns than rows. 

The matrix $\Phi$ represents linear transform of the optical element placed in the camera body. This includes decimation matrix for lenslets, random rows for coded aperture masks, or Gabor wavelets for Angle Sensitive Pixels (ASPs).

\subsection{Reconstruction}

To invert the equation, we can use a pseudo-inverse $\vec{l} = \Phi^{\dagger}\vec{i}$, but this solution does not recover light fields adequately and is sensitive to noise~\cite{Wetzstein:PlenopticMultiplexing:2012}. Linear methods do exist to invert this equation, but sacrifice spatial resolution by stacking image pixels to gain enough measurements so that $\Phi$ is a square matrix. 

To recover the light field at the high spatial image resolution, compressive light field photography~\cite{marwah2013compressive} formulates the following $\ell_1$ minimization problem:
\begin{equation}
\min_{\alpha} || \vec{i} - \Phi D \alpha ||^{2}_{2} + \lambda ||\alpha ||_1
\end{equation}
where the light field can be recovered by performing $l = D\alpha.$ Typically the light fields were split into small patches of 9x9x5x5 $(x,y,\theta,\phi)$ or equivalently sized atoms to be processed by the optimization algorithm. Note that this formulation enforces a sparsity constraint on the number of columns used in dictionary $D$ for the reconstruction. The dictionary $D$ was learned using a set of million light field patches captured by a light field camera and trained using a K-SVD algorithm~\cite{aharon2006img}. To solve this optimization problem, solvers such as ADMM~\cite{boyd2011distributed} were employed. Reconstruction times ranged from several minutes for non-overlapping patch reconstructions to several hours for overlapping patch reconstructions. 

