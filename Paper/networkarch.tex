We first discuss the datasets of light fields we use for simulating coded light field capture along with our training strategy before discussing our network architecture.

\subsection{Light Field Simulation and Training}

% This is the top, 2 column figure
\begin{figure*}
\begin{center}
%\fbox{\rule{0pt}{2in} \rule{.9\linewidth}{0pt}}
\includegraphics[width=\linewidth]{Figures/Training.pdf} 
\end{center}
\caption{Left: Shown are the training sets, both synthetic and the UCSD dataset. Right: The diagram shows an overview of our method to train the network for light-field reconstruction.}
\label{Training}
\end{figure*}

One of the main difficulties for using deep learning for light field reconstructions is the scarcity of available data for training, and the difficulty of getting ground truth (especially for compressive light field measurements). We employ a mixture of simulation and real data to overcome these challenges in our framework (see Figure~\ref{Training} for representative light fields we used). 

\textbf{Synthetic Light Field Archive:}
We use synthetic light fields from the Synthetic Light Field Archive~\cite{Synthetic} which have resolution $(x,y,\theta,\phi) = (593,840,5,5)$. Since the number of parameters for our fully-connected layers would be prohibitively large with the full light field, we split the light fields into $(9,9,5,5)$ patches and reconstruct each local patch. We then stitch the light field back together using overlapping patches to minimize edge effects. 

Our training procedure is as follows. We pick 50,000 random patches from four synthetic light fields, and simulate coded capture by multiplying by $\Phi$ to form images. We then train the network on these images with the labels being the true light field patches. Our training/validation split was 85:15. We finally test our network on a brand new light field never seen before, and report the PSNR as well as visually inspect the quality of the data. In particular, we want to recover parallax in the scenes, i.e. the depth-dependent shift in pixels away from the focal plane as the angular view changes. 

\textbf{Lytro Illum Light Field Dataset:} In addition to synthetic light fields, we utilize real light field captured from a Lytro Illum camera~\cite{LearningViewSynthesis}. To simulate coded capture, we use the same $\Phi$ models for each type of camera and forward model the image capture process, resulting in simulated images that resemble what the cameras would output if they captured that light field. There are a total of 100 light fields, each of size $(364,540,14,14)$. For our simulation purposes, we use only views $[6,10]$ in both $\theta$ and $\phi$, to generate $5x5$ angular viewpoints. We extract 500,000 patches from these light fields of size 9x9x5x5, simulate coded capture, and use 85:15 training/validation split. 

\subsection{Network Architecture}

\begin{figure*}
\includegraphics[height=6.5cm]{Figures/networkarch.pdf}
\caption{The figure shows the 2-branch architecture for light-field reconstruction. Measurements for every patch of size 5 X 5 X 9 X 9 are fed into two streams, one consisting of 6 fully connected and 2 4D convolution layers, and the other consisting of 5 4D convolutional layers. The outputs of the two branches are added with equal weights to obtain the final reconstruction for the patch. Note that the size of filters in all convolution layers is 3 X 3 X 3 X 3.}
\label{fig:network}
\end{figure*}

Our network architecture consists of a two branch network, which one can see in Figure~\ref{fig:network}. In the upper branch,  the 2D input image is vectorized to one dimension, then fed to a series of fully connected layers that form an autoencoder (i.e. alternating contracting and expanding layers). This is followed by one 4D convolutional layer. The lower branch (a 4D CNN) uses a fixed interpolation step of multiplying the input image by $\Phi^T$ to recover a 4D spatio-angular volume, and then fed through a series of 4D convolutional layers with ReLU nonlinearities. Finally the outputs of the two branches are combined with weights of 0.5 to estimate the light field. 

Autoencoders are useful at extracting meaningful information by compressing inputs to hidden states~\cite{vincent2010stacked}, and our autoencoder branch helped to extract parallax (angular views) in the light field. In contrast, our 4D CNN branch utilizes information from the linear reconstruction by interpolating with $\Phi^T$ and then cleaning the result with a series of 4D convolutional layers for improved spatial resolution. Combining the two branches thus gave us good angular recovery along with high spatial resolution. In Figure~\ref{fig:branchcompare}, we show the results of using solely the upper or lower branch of the network versus our two stream architecture. The two branch network gives a 1-2 dB PSNR improvement as compared to the autoencoder or 4D CNN alone, and one can observe the sharper detail in the inlets of the figure.  

For the loss function, we observed that the regular $\ell_2$ loss function gives decent reconstructions, but the amount of parallax and spatial quality recovered in the network at the extreme angular viewpoints were lacking. To remedy this, we employ the following weighted $\ell_2$ loss function:
\begin{equation}
L(l,\hat{l}) = \sum_{\theta,\phi} W(\theta,\phi)\cdot || l(x,y,\theta,\phi) - \hat{l}(x,y,\theta,\phi) ||_2^2,
\end{equation}
where $W(\theta,\phi)$ are weights that increase for higher values of $\theta,\phi$\footnote[1]{ The weight values were picked heuristically for large weights away from the center viewpoint: $W(\theta,\phi) = $ \[ \left( \begin{array}{ccccc}
 \sqrt{5}& 2 & \sqrt{3}& 2 & \sqrt{5} \\
2& \sqrt{3} & \sqrt{2} & \sqrt{3} & 2\\
\sqrt{3} & \sqrt{2} & 1 & \sqrt{2} & \sqrt{3} \\
2 & \sqrt{3} & \sqrt{2} & \sqrt{3} & 2 \\
\sqrt{5} & 2 & \sqrt{3} & 2 & \sqrt{5} \end{array} \right)\]    }. This biases the network to reconstruct the extreme angular views with higher fidelity. 

\subsubsection{Training Details}

All of our networks were trained using Caffe~\cite{jia2014caffe} and using a NVIDIA Titan X GPU. Learning rates were set to $\lambda = .00001$, we used the ADAM solver~\cite{kingma2014adam}, and models were trained for about 60 epochs for 7 hours.  epochs or so. We also finetuned models trained on different $\Phi$ matrices, so that switching the structure of a $\Phi$ matrix did not require training from scratch, but only an additional few hours of finetuning. 

For training, we found the best performance was achieved when we trained each branch separately on the data, and then combined the branches and finetuned the model further on the data. Training from scratch the entire two branch network led to suboptimal performance of 2-3 dB in PSNR, most likely because of local minima in the loss function. 

\begin{figure*}
\begin{center}
\includegraphics[height=6cm]{Figures/branchcompare.pdf}
\end{center}
\caption{The figure shows the reconstruction for two scenes, dragons and seahorse. For both the scenes, we obtain better results in terms of PSNR for the two-stream network than the two individual branches, autoencoder and 4D CNN. This corroborates the need for two-branch network, which marries the benefits of the two branches. }
\label{fig:branchcompare}
\end{figure*}

\subsubsection{GANs}
We tested our network architecture against a state-of-the-art baseline: generative adverserial networks (GANs)~\cite{goodfellow2014generative} which have been shown to work well in image generation and synthesis problems~\cite{pathak2016context, ledig2016photo}. In classical GAN formulations, an uniform random vector $z$ is mapped to an image, and two networks, a generator $G$ and discriminator $D$ are alternatively trained according to a min-max game-theoretic optimization. The goal is for the generator $G$ to generate an image $G(z)$ that fools discriminator $D$.

We use the autoencoder branch as the generator network, and build a discriminator network consisting of 4D convolutions followed by a fully connected layer aimed to decide if a given light field reconstruction was ground truth or a network output. In our case, instead of inputting a random vector, we input the coded measurements $i$ to the generator network. The discriminator network determines if a reconstructed light field is fake or not. One can interpret the discriminator network as classifying the light-field reconstruction from generator network as having parallax or not. While this method has shown considerable improvements on 2D image problems, we found GANs to perform slightly worse than our two branch network by about 2 dB in PSNR, as you can see in Figure~\ref{fig:GAN}. The GAN outputted lower resolution scenes with noticeable color and edge artifacts as one can see in the inlets.

\begin{figure}
\includegraphics[width=\columnwidth]{Figures/GAN.pdf}
\caption{The figure compares the reconstructions for the dragons scene using the two-branch network proposed as well as the autoencoder in conjunction with GAN. Clearly, the quality of the reconstructions for the former (26.77 dB) is greater than that for the latter (24.67 dB). }
\label{fig:GAN}
\end{figure}








